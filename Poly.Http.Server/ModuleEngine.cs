using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using poly.script.poxide;

namespace poly.http {
    public class DynamicModule {
        private DynamicCodeExecutor p_dcExecutor = null;
        private poly.data.ini p_cfConfig = new poly.data.ini();

        public DynamicCodeExecutor Executor {
            get {
                return p_dcExecutor;
            }
        }

        public poly.data.ini Config {
            get {
                return this.p_cfConfig;
            }
        }

        public bool Load(string fileName, string scriptPath) {
            Config.Load(fileName);
            if (Config.Items.Count == 0) {
                return false;
            }
            if (Config["ScriptData", "Enabled"].ToLower() == "true") {
                List<string> RawAsms = new List<string>();
				
                if (Config.Items.ContainsKey("ASM")) {
                    foreach (string VAR in Config["ASM", true].Split('\n')) {
                        if (VAR != null) {
                            RawAsms.Add(VAR);
                        }
                    }
                }

                if (Config.Items.ContainsKey("RequireScript")) {
                    foreach (string VAR in Config["RequireScript", true].Split('\n')) {
                        if (!Load(VAR, scriptPath)) {
                            return false;
                        }
                    }
                }

                if (Config["ScriptData", "Language"] == "VB") {
                    p_dcExecutor = new DynamicCodeExecutor(LANGUAGE.VBnet, RawAsms.ToArray());
                }
                else {
                    p_dcExecutor = new DynamicCodeExecutor(LANGUAGE.CSharp, RawAsms.ToArray());
                }
                p_dcExecutor.Code = System.IO.File.ReadAllText(Environment.CurrentDirectory + scriptPath + Config["ScriptData", "ScriptFile"]);
                p_dcExecutor.Compile();
                return true;
            }
            return false;
        }

        public bool HasErrors {
            get {
                if (this.Executor != null && this.Executor.InterfaceResults != null && this.Executor.InterfaceResults.Errors != null) {
                    return this.Executor.InterfaceResults.Errors.HasErrors;
                }
                return true;
            }
        }

        public string[] Errors {
            get {
                if (!HasErrors) {
                    return new string[1] { "" };
                }
                List<string> erList = new List<string>();

                if (Executor != null && Executor.InterfaceResults != null) {
                    foreach (System.CodeDom.Compiler.CompilerError err in Executor.InterfaceResults.Errors) {
                        erList.Add(string.Format(
                                "[{0}] ERROR ({1}:{2}) : {3}",
                                this.Config["ScriptData", "Name"], err.Line, err.Column, err.ErrorText
                            ));
                    }
                }
                return erList.ToArray();
            }
        }

        public object Call(string function, params object[] Args) {
            return p_dcExecutor.Execute(Config["ScriptData", "MainClass"], function, Args);
        }
    }

    public class ModuleEngine {
        public List<DynamicModule> Scripts = new List<DynamicModule>();

        public DynamicModule GetByName(string name) {
            foreach (DynamicModule script in Scripts) {
                if (script.Config["ScriptData", "Name"].ToLower() == name) {
                    return script;
                }
            }
            return null;
        }

        public void DebugOutput(string bot, string ModName, string Message) {
            Console.WriteLine("{0} MODULE [{1}]: {2}", bot.Split("/\\".ToCharArray()).Last(), ModName, Message);
        }

        public void Initialize(HttpServer Http) {
            Scripts.ForEach(delegate(DynamicModule script) {
                try {
                    script.Call("Dispose", null);
                } catch { }
            });
            Scripts.Clear();
            Http.ModuleList.Clear();
            if (Directory.Exists(Environment.CurrentDirectory + "/Modules/")){
                foreach (string name in Directory.EnumerateFiles(Environment.CurrentDirectory + "/Modules/", "*.hsc", SearchOption.TopDirectoryOnly)){
				    DynamicModule script = new DynamicModule();
                    try {
                        if (!script.Load(name, "/Modules/")) {
						    continue;
                        }
                        if (script.HasErrors) {
                            string[] Errors = script.Errors;
                            foreach (string error in Errors){
							    Console.WriteLine(error);
                            }
                        }
                        else {
                            script.Executor.Instanciate(script.Config["ScriptData", "MainClass"]);
                            script.Call("Initialize", Http);
                            Scripts.Add(script);
                        }
                    } catch (Exception Error) {
                        Console.WriteLine(Error.Message);
					    Console.WriteLine(Error.StackTrace);
                    }
                }
            }
        }
    }
}
