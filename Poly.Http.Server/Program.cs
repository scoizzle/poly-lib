﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

using Poly;
using Poly.Data;
using Poly.Script;
using Poly.Net.Http;

namespace Poly.Http.Server {
    class Program : Application {
        public static FileServer Server = null;
        public static string ConfigName = "Config.json";

        private static Command[] Commands = new Command[] {
            new Command("default",
                (Args) => {
                    Application.Log.Level = Logging.Levels.Error;

                    Server = new FileServer(ConfigName);
                    Server.Start();
                }
            ),
            new Command("start", 
                (Args) => {
                    if (Server == null) {
                        if (File.Exists(ConfigName)){
                            Server = new FileServer(ConfigName);
                            Server.Start();
                        }
                    }
                    else {
                        Server.Start();
                    }
                }
            ),
            new Command("stop",
                (Args) => {
                    if (Server != null)
                        Server.Stop();
                }
            ),
            new Command("restart",
                (Args) =>{
                    if (Server != null)
                        Server.Stop();
                    Server = new FileServer(ConfigName);
                    Server.Start();
                }
            ),
            new Command("-config=\"{Name}\"",
                (Args) => {
                    ConfigName = Args.getString("Name");
                }
            ),
            new Command("exit",
                (Args) => {
                    if (Server != null)
                        Server.Stop();
                    Application.Exit();
                }
            )
        };

        public static void Main(string[] args) {
            Application.UniqueId = string.Join(
                "|",
                "Poly.Http.Server", Environment.UserName, Environment.OSVersion
            ).SHA256();

            Application.SingleInstance = true;

            Application.Init(args, Commands);
            Application.CommandLoop();
		}
    }
}

