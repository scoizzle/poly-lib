﻿// include ~/cs/view.cs
// include ~/cs/config.cs
// include ~/cs/database.cs
// include ~/cs/group.cs
// include ~/cs/user.cs
// include ~/cs/session.cs

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;
using Poly.Http;
using Poly.Template;

namespace WWW {
    public partial class Layout : Bootstrap {
        public Database GetDatabase(Request Request) {
            if (Request["Database"] == null) {
                var Db = new Database(
                    Config.Database.getString("Domain"),
                    Config.Database.getString("Name"),
                    Config.Database.getString("User"),
                    Config.Database.getString("Pass")
                );

                Request["Database"] = Db;

                Db.Open();

                return Db;
            }

            var eDb = Request.getObject<Database>("Database");

            if (eDb.Connection.State != System.Data.ConnectionState.Open) {
                eDb.Open();
            }

            return eDb;
        }

        public void Page(Request Request, Session Session, jsObject Variables, params string[] Content) {
            Request.Print(
                Variables.Template(@"
                    <!DOCTYPE html>
                    <!-- {PageUrl} -->
                    <html>
                    <head>
                        <title>{PageTitle}</title>
                    </head>
                    </head>
                    <body>
                ")
            );

            Request.Print(
                Variables.Template(
                    string.Join("", Content)
                )
            );

            Request.Print(
                Variables.Template(@"
                    </body>
                    </html>
                ")
            );
        }

        //    Request.Print(
        //        HTML(
        //            Head(
        //                Title(PageTitle),
        //                DefaultStyles()
        //            ),
        //            Body(
        //                Nav(
        //                    Config.Site.getString("Name"),
        //                    PageUrl,
        //                    NavUserForm(Request, GetDatabase(Request), Session)
        //                ),
        //                Content(
        //                    Elements
        //                ),
        //                DefaultScripts()
        //            )
        //        )
        //    );
        //}

        //public string Nav(string SiteName, string CurrentUrl, params string[] Elements) {
        //    return NavBar(
        //        Brand(SiteName),
        //        NavCollapse(
        //            NavList(
        //                NavLinks(CurrentUrl)
        //            ),
        //            string.Join("", Elements)
        //        )
        //    );
        //}

        //public string NavLinks(string CurrentUrl) {
        //    StringBuilder Output = new StringBuilder();
        //    var Navigation = Config.Site.getObject("Navigation");

        //    foreach (string Text in Navigation.Keys) {
        //        string Url = Navigation.getString(Text, "Url");
        //        bool bIcon = Navigation.getBool(Text, "Icon");

        //        var Display = Link(
        //            Url,
        //            bIcon ? 
        //                IconWhite(Text) :
        //                Text
        //        );

        //        Output.Append(
        //            Url.Compare(CurrentUrl) ?
        //                ListItemActive(Display) :
        //                ListItem(Display)
        //        );
        //    }

        //    return Output.ToString();
        //}

        //public string NavUserForm(Request Req, Database Db, Session Session) {
        //    if (Session.Data.ContainsKey("UID")) {
        //        User usr = User.getUserById(Db, Session.Data.getString("UID"));

        //        Session.LastAccessTime = DateTime.Now;
        //        Session.Update();

        //        return ul(
        //            ("'class':'nav pull-right'").ToJsObject(),
        //            li(("'class':'dropdown active'").ToJsObject(),
        //                DropdownToggle(
        //                    usr.Display, IconWhite("chevron-down")
        //                ),
        //                DropdownMenu(
        //                    ListItem(
        //                        Link(
        //                            "/account/manage/", "Account"
        //                        )
        //                    ),
        //                    ListItem(
        //                        Link(
        //                            "/account/logout/", "Logout"
        //                        )                                    
        //                    )
        //                )
        //            )
        //        );
        //    } else {
        //        return NavForm("/account/", "POST",
        //            Input("text", "Username", "'placeholder':'Username', 'class':'span2'"),
        //            Input("password", "Password", "'placeholder':'Password', 'class':'span2'"),
        //            Input("submit", "Login", "'value':'Login', 'class':'btn'"),
        //            Button("/account/", "Register")
        //        );
        //    }
        //}
    }
}
