﻿// embed ~/Elements.json

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Poly;
using Poly.Data;
using Poly.Http;

namespace WWW {
    public class Bootstrap {
        public List<string> StyleSheets = new List<string>() {
            "/css/bootstrap.min.css",
            "/css/global.css",
            "/css/bootstrap-responsive.min.css"
        };

        public List<string> ScriptFiles = new List<string>() {
            "//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js",
            "/js/bootstrap.min.js"
        };

        public enum Types {
            Default,
            Primary,
            Info,
            Success,
            Warning,
            Error,
            Danger
        };

        public string TypeString(Types Type) {
            switch (Type) {
                default:
                case Types.Primary:
                    return "primary";

                case Types.Info:
                    return "info";

                case Types.Success:
                    return "success";

                case Types.Warning:
                    return "warning";

                case Types.Error:
                    return "error";

                case Types.Danger:
                    return "danger";
            }
        }

        public string Element(string Type) {
            return string.Format("<{0}/>", Type);
        }

        public string Element(string Type, params string[] Children) {
            return string.Format("<{0}>{1}</{0}>", Type, string.Join("", Children));
        }

        public string Element(string Type, jsObject Attributes) {
            var Output = new StringBuilder();

            Output.Append("<").Append(Type);

            if (!Attributes.IsEmpty) {
                var Attr = new StringBuilder();

                foreach (var Item in Attributes) {
                    Output.Append(' ').AppendFormat("{0}=\"{1}\"", Item.Key, Item.Value);
                }

                Output.Append(Attr.ToString());
            }

            Output.Append("/>");

            return Output.ToString();
        }

        public string Element(string Type, jsObject Attributes, params string[] Children) {
            var Output = new StringBuilder();

            Output.AppendFormat("<{0}", Type);

            if (!Attributes.IsEmpty) {
                var Attr = new StringBuilder();

                foreach (var Item in Attributes) {
                    Output.AppendFormat(" {0}=\"{1}\"", Item.Key, Item.Value);
                }

                Output.Append(Attr.ToString());
            }

            Output.Append(">");

            Output.Append(string.Join("", Children));

            Output.AppendFormat("</{0}>", Type);

            return Output.ToString();
        }

        public string HTML(params string[] Elements) {
            return "<!DOCTYPE html>" + Element("html", Elements);
        }

        public string Head(params string[] Elements) {
            return Element("head", Elements);
        }

        public string Title(string Title) {
            return Element("title", Title);
        }

        public string StyleLink(string Href) {
            return Element("link", ("'rel':'stylesheet', 'href':'" + Href + "'").ToJsObject());
        }

        public string DefaultStyles() {
            var Output = new StringBuilder();

            foreach (var Href in StyleSheets) {
                Output.Append(
                    StyleLink(Href)
                );
            }

            return Output.ToString();
        }

        public string ScriptLink(string Href) {
            return Element("script", ("'src':'" + Href + "'").ToJsObject(), "");
        }

        public string DefaultScripts() {
            var Output = new StringBuilder();

            foreach (var Href in ScriptFiles) {
                Output.Append(
                    ScriptLink(Href)
                );
            }

            return Output.ToString();
        }

        public string Body(params string[] Elements) {
            return Element("body", Elements);
        }

        public string br() {
            return Element("br");
        }

        public string Caret() {
            return span(("'class':'caret'").ToJsObject());
        }

        public string p(params string[] Elements) {
            return Element("p", "", Elements);
        }

        public string Form(string Url = "#", string Method = "GET", params string[] Elements) {
            return Element("form", ("'action': '" + Url + "', 'method': '" + Method + "'").ToJsObject(), Elements);
        }

        public string Input(string Type, string Name, jsObject Attributes, params string[] Elements) {
            Attributes["type"] = Type;
            Attributes["name"] = Name;
            return Element("input", Attributes, Elements);
        }

        public string div(jsObject Attributes, params string[] Elements) {
            return Element("div", Attributes, Elements);
        }

        public string a(jsObject Attributes, params string[] Elements) {
            return Element("a", Attributes, Elements);
        }

        public string span(jsObject Attributes, params string[] Elements) {
            return Element("span", Attributes, Elements);
        }

        public string ul(jsObject Attributes, params string[] Elements) {
            return Element("ul", Attributes, Elements);
        }

        public string li(jsObject Attributes, params string[] Elements) {
            return Element("li", Attributes, Elements);
        }

        public string ListItem(params string[] Elements) {
            return li("", Elements);
        }

        public string ListItemActive(params string[] Elements) {
            return li(("'class':'active'").ToJsObject(), Elements);
        }

        public string Hero(params string[] Elements) {
            return div("'class':'hero-unit'".ToJsObject(), Elements);
        }

        public string Content(params string[] Elements) {
            return div("'class':'container content'".ToJsObject(), Elements);
        }

        public string MainContent(string Id, params string[] Elements) {
            return div(("'class':'main-content', 'id':'" + Id + "'").ToJsObject(), Elements);
        }

        public string Link(string Href, params string[] Elements) {
            return a(("'href':'" + Href + "'").ToJsObject(), Elements);
        }

        public string Brand(params string[] Elements) {
            return a(("'class':'brand', 'href':'/'").ToJsObject(), Elements);
        }

        public string Icon(string Name) {
            return Element("i", ("'class':'icon-" + Name.ToLower() + "'").ToJsObject(), "");
        }

        public string IconWhite(string Name) {
            return Element("i", ("'class':'icon-" + Name.ToLower() + " icon-white'").ToJsObject(), "");
        }

        public string NavBar(params string[] Elements) {
            List<string> Elems = new List<string>(Elements);

            Elems.Insert(0, 
                a(("'class':'btn btn-navbar', 'data-toggle':'collapse', 'data-target':'.nav-collapse'").ToJsObject(),
                    span(("'class':'icon-bar'").ToJsObject()).x(3)
                )
            );

            return div(("'class':'navbar navbar-inverse navbar-fixed-top'").ToJsObject(),
                        div(("'class':'navbar-inner'").ToJsObject(),
                            div(("'class':'container'").ToJsObject(),
                                Elems.ToArray()
                            )
                        )
                    );
        }

        public string NavList(params string[] Elements) {
            return ul(("'class':'nav'").ToJsObject(), Elements);
        }

        public string NavCollapse(params string[] Elements) {
            return div(("'class':'nav-collapse collapse'").ToJsObject(), Elements);
        }

        public string NavForm(string Url = "#", string Method = "GET", params string[] Elements) {
            return Element("form", ("'action': '" + Url + "', 'method': '" + Method + "', 'class': 'navbar-form pull-right'").ToJsObject(), Elements);
        }

        public string NavCaret() {
            return Element("b", ("'class':'caret'").ToJsObject(), "");
        }

        public string SideNavBar(params string[] Elements) {
            return div(("'class':'well sidebar-nav'").ToJsObject(), Elements);
        }

        public string SideNavList(params string[] Elements) {
            return ul(("'class':'nav nav-list'").ToJsObject(), Elements);
        }

        public string SideNavHeader(params string[] Elements) {
            return li(("'class':'nav-header'").ToJsObject(), Elements);
        }

        public string Row(params string[] Elements) {
            return div(("'class':'row'").ToJsObject(), Elements);
        }

        public string FluidRow(params string[] Elements) {
            return div(("'class':'row-fluid'").ToJsObject(), Elements);
        }

        public string Column(int Width, params string[] Elements) {
            return div(("'class':'span" + Width.ToString() + "'").ToJsObject(), Elements);
        }

        public string DropdownMenu(params string[] Elements) {
            return ul(("'class':'dropdown-menu'").ToJsObject(), Elements);
        }

        public string DropdownToggle(params string[] Elements) {
            return a(("'class': 'dropdown-toggle', 'data-toggle':'dropdown', 'href':'#'").ToJsObject(), Elements);
        }

        public string Button(string Href, params string[] Elements) {
            return Button(Types.Default, Href, Elements);
        }

        public string Button(Types Type, string Href, params string[] Elements) {
            var Attributes = ("'href':'" + Href + "', 'class':'btn");

            if (Type != Types.Default) {
                Attributes += " btn-" + TypeString(Type);
            }

            Attributes += "'";

            return a(Attributes.ToJsObject(), Elements);
        }

        public string ButtonDropdown(string Href, params string[] Elements) {
            return ButtonDropdown(Types.Default, Href, Elements);
        }

        public string ButtonDropdown(Types Type, string Href, params string[] Elements) {
            var Attributes = ("'href':'" + Href + "', 'class':'btn");

            if (Type != Types.Default) {
                Attributes += " btn-" + TypeString(Type);
            }

            Attributes += " dropdown-toggle', 'data-toggle':'dropdown'";

            return a(Attributes.ToJsObject(), Elements);
        }

        public string ButtonGroup(params string[] Elements) {
            return div(("'class':'btn-group'").ToJsObject(), Elements);
        }

        public string Alert(params string[] Elements) {
            return Alert(Types.Default, Elements);
        }

        public string Alert(Types Type, params string[] Elements) {
            var Attributes = "'class':'alert";

            if (Type != Types.Default) {
                Attributes += " alert-" + TypeString(Type);
            }

            Attributes += "'";

            return div(Attributes.ToJsObject(), Elements);
        }

        public string AlertBlock(params string[] Elements) {
            return AlertBlock(Types.Default, Elements);
        }

        public string AlertBlock(Types Type, params string[] Elements) {
            var Attributes = "'class':'alert";

            if (Type != Types.Default) {
                Attributes += " alert-" + TypeString(Type);
            }

            Attributes += " alert-block'";

            return div(Attributes.ToJsObject(), Elements);
        }

        public string AlertClose(params string[] Elements) {
            return Element("button", ("'type':'button', 'class':'close', 'data-dismiss':'alert'").ToJsObject(), Elements);
        }
    }
}