﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;
using Poly.Template;
using Poly.Http;

namespace WWW {
    public static class ResultViewExtension {
        public static bool LoadView(this Request This, string ViewName, jsObject Variables) {
            var FileName = This.Host.getString("WWW") + "/view/" + ViewName + ".htm";

            if (!File.Exists(FileName))
                return false;

            This.Print(
                Variables.Template(
                    File.ReadAllText(
                        FileName
                    )
                )
            );

            return true;
        }
    }
}
