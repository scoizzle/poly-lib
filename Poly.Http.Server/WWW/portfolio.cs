﻿// namespace WWW
// class Portfolio
// main Main

// include ~/cs/layout.cs

using System;
using System.Collections.Generic;
using System.Text;

using Poly;
using Poly.Data;
using Poly.Http;

namespace WWW {
    public class Portfolio : Layout {
        public void Main(Request Request) {
            Database Data = GetDatabase(Request);

            try {
                Session Session = WWW.Session.getSessionFromRequest(Data, Request);

                Page(Request, Session,  "Scot Murphy - Portfolio", "/portfolio/",
                    Row(
                        Column(12,
                            p(
                                "I am a Software Developer and Server Administrator from Wichita, Kansas. Born in July, 1994.",
                                "I know a few different languages and have worked with many flavors of linux distrobutions."
                            )
                        )
                    )
                );
            } catch (Exception Error) {
                Application.Info("WWW.About", Error.ToString());
            }

            Data.Close();

            Request.CleanHTML();
            Request.Finish();
        }
    }
}
