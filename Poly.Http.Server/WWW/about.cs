﻿// namespace WWW
// class About
// main Main

// include ~/cs/layout.cs

using System;
using System.Collections.Generic;
using System.Text;

using Poly;
using Poly.Data;
using Poly.Http;

namespace WWW {
    public class About : Layout {
        public void Main(Request Request) {
            Database Data = GetDatabase(Request);

            try {
                Session Session = WWW.Session.getSessionFromRequest(Data, Request);

                Page(Request, Session,  "Scot Murphy - About Me", "/about/",
                    Row(
                        Column(12,
                            p(
                                "I am a Software Developer and Server Administrator from Wichita, Kansas. Born in July, 1994.",
                                "I know a few different languages and have worked with many distrobutions of linux."
                            )
                        )
                    )
                );
            } catch (Exception Error) {
                Application.Info("WWW.About", Error.ToString());
            }

            Request.CleanHTML();
            Request.Finish();
        }
    }
}
