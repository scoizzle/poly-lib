﻿// namespace WWW
// class Resume
// main Main

// include ~/cs/layout.cs

using System;
using System.Collections.Generic;
using System.Text;

using Poly;
using Poly.Data;
using Poly.Http;

namespace WWW {
    public class Resume : Layout {
        public void Main(Request Request) {
            Database Data = GetDatabase(Request);

            try {
                Session Session = WWW.Session.getSessionFromRequest(Data, Request);

                Page(Request, Session,  "Scot Murphy - Resume", "/resume/",
                    Row(
                        Column(12,
                            Hero(
                                p(
                                    Element("strong", "",
                                        Element("span", "'style':'font-size: 20px;'", "Hi,"),
                                        "and welcome to my Portfolio!"
                                    )
                                ),
                                p(
                                    "I am a C# Desktop and Web backend developer. This website is a show case for a few projects of mine.",
                                    "<br/>",
                                    "For a list of the projects I've had influence on, please visit the ",
                                    Link("/projects/", "projects"),
                                    " page."
                                )
                            )
                        )
                    ),
                    Element("hr", "")
                );
            } catch (Exception Error) {
                Application.Info("WWW.About", Error.ToString());
            }

            Data.Close();

            Request.CleanHTML();
            Request.Finish();
        }
    }
}
