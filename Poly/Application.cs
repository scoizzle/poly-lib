﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Diagnostics;

using Poly.Data;

namespace Poly {
    public partial class Application {
		public static bool Running = false;
        public static bool SingleInstance = false;
        public static Logging Log = new Logging();

        public static string UniqueId = "";
        private static Mutex AppMutex = null;

        private static List<Command> CommandList = new List<Command>() {
            new Command("--debug={Level}",
                (Args) => {
                    Application.Log.Active = true;
                    Application.Log.Level = Args.getInt("Level");
                }
            )
        };

		public static void Init(string[] Args, params Command[] Commands) {
            Application.CommandList.AddRange(Commands);

            if (SingleInstance && !string.IsNullOrEmpty(UniqueId)) {
                bool Result = false;
                AppMutex = new Mutex(true, UniqueId, out Result);

                if (!Result) {
                    Application.Log.Error("Application already running...");
                    Application.Exit(0);
                }
            }

            if (Args.Length == 0) {
                HandleCommand("default");
            }
            else {
                foreach (var Cmd in Args) {
                    HandleCommand(Cmd);
                }
            }

            Application.Log.Info("Application initializing...");
            
            int workerThreads, completionThreads;

            ThreadPool.GetMaxThreads(out workerThreads, out completionThreads);
            ThreadPool.SetMaxThreads(workerThreads, completionThreads * Environment.ProcessorCount);

			Running = true;
            Application.Log.Info("Application running...");
		}

        public static void CommandLoop() {
            while (Running) {
                HandleCommand( 
                    Console.ReadLine() 
                );

            }
        }

        public static bool HandleCommand(string Command) {
            foreach (var Cmd in CommandList.AsParallel()) {
                var Args = Command.Match(Cmd.String);

                if (Args != null) {
                    Cmd.Handler(Args);
                    return true;
                }
            }
            return false;
        }

        public static void PrintCommandList() {
            CommandList.ForEach((Cmd) => {
                Console.WriteLine(Cmd.ToString());
            });
        }

		public static void Exit(int Status = 0) {
            Log.Info("Applcation Exiting...");

            Running = false;
			Thread.Sleep(1000);
			Environment.Exit(0);
		}
	}
}
