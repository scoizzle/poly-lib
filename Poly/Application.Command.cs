﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly.Data;

namespace Poly {
    public class Command {
        public string String = "";
        public string Description = "";
        public Action<jsObject> Handler = null;

        public Command(string String, Action<jsObject> Handler) {
            this.String = String;
            this.Handler = Handler;
        }

        public Command(string String, string Description, Action<jsObject> Handler) {
            this.String = String;
            this.Description = Description;
            this.Handler = Handler;
        }

        public override string ToString() {
            return string.IsNullOrEmpty(Description) ?
                String :
                string.Format("{0}: {1}", String, Description);
        }
    }
}
