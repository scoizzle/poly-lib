﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Poly.Data {
    public class jsObject : Dictionary<string, object>, IDisposable {
        protected static Type[] ParseTypes = new Type[] {
            typeof(string)
        };

        public string KeySeperator = ".";
        public bool IsArray = false;

        public jsObject(int InitialCapacity, string Json = "") : base(InitialCapacity) {
            
        }

        public jsObject(string Json = "") : this(Environment.ProcessorCount, Json) {
        }

        public virtual object this[params string[] Key] {
            get {
                if (Key.Length == 1) {
                    if (Key[0].Contains(KeySeperator)) {
                        return Get(Key[0].Split(KeySeperator));
                    }
                    else if (this.ContainsKey(Key[0])) {
                        return base[Key[0]];
                    }
                    else {
                        return null;
                    }
                }

                return Get(Key);
            }
            set {
                if (Key.Length == 1) {
                    if (Key.Contains(KeySeperator)) {
                        Set(Key[0].Split(KeySeperator), value);
                    }
                    else {
                        if (value != null)
                            base[Key[0]] = value;
                        else if (base.ContainsKey(Key[0]))
                            base.Remove(Key[0]);
                    }
                }
                else {
                    Set(Key, value);
                }
            }
        }

        public bool IsEmpty {
            get {
                return this.Count == 0;
            }
        }

        public jsObject Base {
            get {
                return (this as jsObject);
            }
        }

        public R Get<R>(string[] KeyList) {
            jsObject Current = this;

            for (int Index = 0; Index < KeyList.Length; Index++) {
                string Key = KeyList[Index];

                if (string.IsNullOrEmpty(Key))
                    continue;

                if (!Current.ContainsKey(Key)) {
                    break;
                }

                if (Index == KeyList.Length - 1) {
                    object Value = Current[Key];

                    if (Value is R) {
                        return (R)Value;
                    }
                    try {
                        var Method = typeof(R).GetMethod("Parse", ParseTypes);

                        if (Method != null) {
                            return (R)Method.Invoke(null, new object[] { Value });
                        }

                        return (R)Value;
                    }
                    catch {
                    }
                }
                else if (Current[Key] is jsObject) {
                    Current = (Current[Key] as jsObject);
                }
                else {
                    break;
                }    
            }

            return default(R);
        }

        public object Get(string[] KeyList) {
            return Get<object>(KeyList);
        }

        public object Get(string Key) {
            if (Key.Contains(KeySeperator))
                return Get(Key.Split(KeySeperator));
            if (this.ContainsKey(Key))
                return base[Key];
            return null;
        }

        public bool Set<R>(string[] KeyList, R Value) {
            jsObject Current = this;

            if (KeyList.Length == 0 && Value is jsObject) {
                (Value as jsObject).CopyTo(Current);
                return true;
            }

            for (int Index = 0; Index < KeyList.Length; Index++) {
                string Key = KeyList[Index];

                if (string.IsNullOrEmpty(Key))
                    continue;

                if (Index == KeyList.Length - 1) {
                    Current[Key] = Value;
                    return true;
                }
                else if (!Current.ContainsKey(Key)) {
                    Current[Key] = new jsObject();
                }
                else if (Current[Key] is jsObject) {
                    Current = (Current[Key] as jsObject);
                }
                else {
                    break;
                }
            }

            return false;
        }

        public bool Set(string[] KeyList, object Value) {
            return Set<object>(KeyList, Value);
        }

        public bool Set(string Key, object Value) {
            this[Key] = Value;
            return true;
        }

        public void Add(object Value) {
            this[Count.ToString()] = Value;
        }

        public void Dispose() {
            Clear();
        }

        public void CopyTo(jsObject Object) {
            foreach (var Item in this.ToArray().AsParallel()) {
                Object[Item.Key] = Item.Value;
            }
        }

        public virtual void ForEach(Action<string, object> Action) {
            this.AsParallel().ForAll((Pair) => {
                Action(Pair.Key, Pair.Value);
            });
        }

        public bool IsValid<R>(params string[] Key) {
            object Object = this[Key];

            if (Object == null)
                return false;

            return (Object is R);
        }

        public R getObject<R>(params string[] Key) {
            return Get<R>(Key);
        }

        public R Options<R>(params string[] Key) {
            for (int Index = 0; Index < Key.Length; Index++) {
                if (this.ContainsKey(Key[Index]))
                    return getObject<R>(Key[Index]);
            }

            return default(R);
        }

        public R Search<R>(string Key, bool IgnoreCase = true) {
            foreach (var Pair in this.AsParallel()) {
                if (Pair.Value is R) {
                    if (Key.Compare(Pair.Key, IgnoreCase)) {
                        return (R)Pair.Value;
                    }
                }
            }

            return default(R);
        }

        public int getInt(params string[] Key) {
            return getObject<int>(Key);
        }

        public double getDouble(params string[] Key) {
            return getObject<double>(Key);
        }

        public bool getBool(params string[] Key) {
            return getObject<bool>(Key);
        }

        public string getString(params string[] Key) {
            return getObject<string>(Key);
        }

        public jsObject getObject(params string[] Key) {
            return getObject<jsObject>(Key);
        }

        private object getObjectValue(string val) {
            int ntVal;
            if (int.TryParse(val, out ntVal)) {
                return ntVal;
            }

            double dbVal;
            if (double.TryParse(val, out dbVal)) {
                return dbVal;
            }

            bool blVal;
            if (bool.TryParse(val, out blVal)) {
                return blVal;
            }

            long lnVal;
            if (long.TryParse(val, out lnVal)) {
                return lnVal;
            }

            return val;
        }

        private bool _Raw(string Text, ref int Index, params string[] Key) {
            var Token = Text.ComesFirst(",", "}", "]");
            var SubIndex = Text.IndexOf(Token, Index);

            if (SubIndex == -1)
                SubIndex = Text.Length;

            var Sub = Text.Substring(Index, SubIndex - Index);

            Set(Key, getObjectValue(Sub.Trim()));

            Index += Sub.Length;

            return true;
        }

        private bool _String(string Text, ref int Index, params string[] Key) {
            if (Text[Index] == '"') {
                var Sub = Text.Substring("\"", "\"", Index, true);

                Index += Sub.Length;

                Set(Key, Sub.Substring(1, Sub.Length - 2).Descape());
            }
            else if (Text[Index] == '\'') {
                var Sub = Text.Substring("'", "'", Index, true);

                Index += Sub.Length;

                Set(Key, Sub.Substring(1, Sub.Length - 2).Descape());
            }
            else {
                return false;
            }

            return true;
        }

        private bool _Array(string Text, ref int Index, params string[] Key) {
            var SubIndex = 0;
            var This = default(jsObject);

            if (Key.Length == 0) {
                This = this;
            }
            else {
                This = new jsObject() {
                    IsArray = true
                };

                Set(Key, This);
            }

            Text = Text.FindMatchingBrackets("[", "]", Index, true);

            Index += Text.Length;

            Text = Text.Substring(1, Text.Length - 2);

            do {
                if (Text[SubIndex] == ',') {
                    SubIndex++;
                }

                while (SubIndex < Text.Length && char.IsWhiteSpace(Text[SubIndex]))
                    SubIndex++;

                if (SubIndex == Text.Length)
                    break;


                var X = Text[SubIndex];
                var Name = This.Count.ToString();

                if (X == '"') {
                    if (!This._String(Text, ref SubIndex, Name))
                        return false;
                }
                else if (X == '\'') {
                    if (!This._String(Text, ref SubIndex, Name))
                        return false;
                }
                else if (X == '{') {
                    if (!This._Object(Text, ref SubIndex, Name))
                        return false;
                }
                else if (X == '[') {
                    if (!This._Array(Text, ref SubIndex, Name))
                        return false;
                }
                else {
                    if (!This._Raw(Text, ref SubIndex, Name))
                        return false;
                }
            }
            while (SubIndex < Text.Length);

            return true;
        }

        private bool _Object(string Text, ref int Index, params string[] Key) {
            var SubIndex = 0;
            var This = default(jsObject);

            if (Key.Length == 0) {
                This = this;
            }
            else {
                This = new jsObject();
            }

            Text = Text.FindMatchingBrackets("{", "}", Index, true);

            Index += Text.Length;

            Text = Text.Substring(1, Text.Length - 2);

            do {
                if (Text[SubIndex] == ',') {
                    SubIndex++;
                }

                while (SubIndex < Text.Length && char.IsWhiteSpace(Text[SubIndex]))
                    SubIndex++;

                if (SubIndex == Text.Length)
                    break;

                if (!This._NamedValue(Text, ref SubIndex)) {
                    return false;
                }
            }
            while (SubIndex < Text.Length);

            OnParseObject(This, Key);

            return true;
        }

        private bool _NamedValue(string Text, ref int Index) {
            var Name = "";
            var X = Text[Index];

            if (X == '"') {
                Name = Text.FindMatchingBrackets("\"", "\"", Index);

                Index += Name.Length + 2;
            }
            else if (X == '\'') {
                Name = Text.FindMatchingBrackets("'", "'", Index);

                Index += Name.Length + 2;
            }
            else {
                Name = Text.Substring("", ":", Index, false).Trim();
            }

            Index = Text.IndexOf(':', Index);

            if (Index == -1)
                return false;
            else {
                Index++;

                while (char.IsWhiteSpace(Text[Index]))
                    Index++;
            }


            X = Text[Index];

            if (X == '"' || X == '\'') {
                return _String(Text, ref Index, Name);
            }
            else if (X == '{') {
                return _Object(Text, ref Index, Name);
            }
            else if (X == '[') {
                return _Array(Text, ref Index, Name);
            }
            else {
                return _Raw(Text, ref Index, Name);
            }
        }

        public virtual void OnParseObject(jsObject Object, params string[] Key) {
            Set(Key, Object);
        }

        public bool Parse(string Text) {
            int Index = 0;

            if (Text == null) {
                return false;
            }
            else {
                Text = Text.Trim();
            }
            if (Text == string.Empty) {
                return true;
            }

            if (Text.StartsWith("{") && Text.EndsWith("}")) {
                return _Object(Text, ref Index);
            }
            else if (Text.StartsWith("[") && Text.EndsWith("]")) {
                return _Array(Text, ref Index);
            }
            else {
                return _Object("{ " + Text + " }", ref Index);
            }
        }

        public override string ToString() {
            return ToString(false);
        }

        public virtual string ToString(bool HumanFormat = false) {
            return jsObject.Stringify(this, HumanFormat);
        }

        public static string Stringify(jsObject This, bool HumanFormat, int Reserved = 1) {
            string CurTab = new string('\t', Reserved);
            StringBuilder Output = new StringBuilder();

            if (This.IsArray) {
                Output.Append("[");
            }
            else {
                Output.Append("{");
            }

            if (HumanFormat) {
                Output.AppendLine().Append(CurTab);
            }

            foreach (KeyValuePair<string, object> Pair in This) {
                if (!Pair.Key.IsNumeric()) {
                    Output.AppendFormat("'{0}':", Pair.Key);
                }

                if (Pair.Value is jsObject) {
                    Output.Append(
                        jsObject.Stringify((Pair.Value as jsObject), HumanFormat, Reserved + 1)
                    );
                }
                else if (Pair.Value is string) {
                    Output.AppendFormat("'{0}'", Pair.Value.ToString().Escape());
                }
                else {
                    Output.Append(Pair.Value);
                }

                if (This.Count > 1 && Pair.Key != This.Last().Key) {
                    Output.Append(',');
                }
                else {
                    CurTab = CurTab.Substring(0, CurTab.Length - 1);
                }

                if (HumanFormat) {
                    Output.Append(Environment.NewLine).Append(CurTab);
                }
            }

            if (This.IsArray) {
                Output.Append("]");
            }
            else {
                Output.Append("}");
            }


            return Output.ToString();
        }

        public static jsFile FromFile(string fileName) {
            jsFile Object = new jsFile();

            if (Object.Load(fileName)) {
                return Object;
            }

            return default(jsFile);
        }

        public static jsObject FromUrl(string Url, string Data = "") {
            System.Net.WebClient Client = new System.Net.WebClient();
            if (string.IsNullOrEmpty(Data)) {
                Data = Client.DownloadString(Url);
            }
            else {
                Data = Client.UploadString(Url, Data);
            }

            jsObject Object = new jsObject();

            if (Object.Parse(Data)) {
                return Object;
            }

            return default(jsObject);
        }

        public static jsObject FromEmbeded(string Name) {
            Data.jsObject Obj = "";
            var asm = Assembly.GetCallingAssembly();

            try {
                using (StreamReader Reader = new StreamReader(asm.GetManifestResourceStream(Name))) {
                    Obj = Reader.ReadToEnd();
                }
            }
            catch {
                return "";
            }

            return Obj;
        }

        public static implicit operator jsObject(string Text) {
            jsObject Object = new jsObject();

            if (Object.Parse(Text)) {
                return Object;
            }

            return default(jsObject);
        }
    }

    public class jsObject<T> : jsObject where T : class {
        public new T this[params string[] Key] {
            get {
                return (T)base[Key];
            }
            set {
                base[Key] = value;
            }
        }

        public void ForEach(Action<string, T> Action) {
            foreach (var Pair in this.ToArray().AsParallel()) {
                Action(Pair.Key, this[Pair.Key]);
            }
        }
    }


    //public class jsObject : Dictionary<string, object> {
    //    protected static Type[] ParseTypes = new Type[] {
    //        typeof(string)
    //    };

    //    public string KeySeperator = ".";
    //    public bool IsObject = false, IsArray = false;

    //    public static bool HumanFormatting = false;

    //    public jsObject(int initCapacity, string Json = "") : base(initCapacity) {
    //        if (!string.IsNullOrEmpty(Json))
    //            Parse(Json);
    //    }

    //    public jsObject(string Json = "") {
    //        if (!string.IsNullOrEmpty(Json))
    //            Parse(Json);
    //    }
        
    //    public bool IsEmpty {
    //        get {
    //            return this.Count == 0;
    //        }
    //    }

    //    public jsObject Base {
    //        get {
    //            return (jsObject)this;
    //        }
    //    }

    //    public string Hash {
    //        get {
    //            return this.ToString(false).MD5();
    //        }
    //    }

    //    public object getObjectValue(string val) {
    //        int ntVal;
    //        if (int.TryParse(val, out ntVal)) {
    //            return ntVal;
    //        }

    //        double dbVal;
    //        if (double.TryParse(val, out dbVal)) {
    //            return dbVal;
    //        }

    //        bool blVal;
    //        if (bool.TryParse(val, out blVal)) {
    //            return blVal;
    //        }

    //        long lnVal;
    //        if (long.TryParse(val, out lnVal)) {
    //            return lnVal;
    //        }

    //        return val;
    //    }

    //    private object get(string[] KeyList) {
    //        var subj = this;

    //        for (var Index = 0; Index < KeyList.Length; Index++) {
    //            var Key = KeyList[Index];

    //            if (subj[Key] == null)
    //                break;

    //            if (Index == KeyList.Length - 1) {
    //                return subj[Key];
    //            }
    //            else if (subj[Key] is jsObject) {
    //                subj = subj[Key] as jsObject;
    //            }
    //            else {
    //                break;
    //            }
    //        }

    //        return null;
    //    }

    //    private void set(string[] KeyList, object Value, bool failSafe = true) {
    //        var subj = this;

    //        if (KeyList.Length == 1) {
    //            subj[KeyList[0]] = Value;
    //            return;
    //        }

    //        for (var Index = 0; Index < KeyList.Length; Index++) {
    //            var Key = KeyList[Index];

    //            if (Index == KeyList.Length - 1) {
    //                subj[Key] = Value;
    //            }
    //            else if (!subj.ContainsKey(Key)) {
    //                if (failSafe) {
    //                    subj[Key] = new jsObject();
    //                    subj = subj[Key] as jsObject;
    //                }
    //                else {
    //                    throw new Exception("jsObject Set fail safe error!");
    //                }
    //            }
    //            else if (subj[Key] is jsObject) {
    //                subj = subj[Key] as jsObject;
    //            }
    //            else {
    //                throw new Exception("jsObject Set error!");
    //            }                    
    //        }
    //    }

    //    public void Add(object Value) {
    //        this.Add(this.Count.ToString(), Value);
    //    }

    //    public new object this[string Key] {
    //        get {
    //            Key = Key.Escape();
    //            if (Key.Contains(KeySeperator)) {
    //                return get(Key.Split(KeySeperator));
    //            }
    //            else if (this.ContainsKey(Key)){
    //                return base[Key];
    //            }
    //            return null;
    //        }
    //        set {
    //            Key = Key.Escape();
    //            if (!Key.Contains(KeySeperator)) {
    //                base[Key] = value;
    //            }
    //            this[Key.Split(KeySeperator)] = value;
    //        }
    //    }

    //    public object this[params string[] Key] {
    //        get {
    //            if (Key.Length == 0)
    //                return null;

    //            if (Key.Length == 1 && this.ContainsKey(Key.First())) {
    //                return base[Key.First()];
    //            } else {
    //                return get(Key);
    //            }
    //        }
    //        set {
    //            if (Key.Length == 0)
    //                return;

    //            if (Key.Length == 1) {
    //                base[Key.First()] = value;
    //            } else {
    //                set(Key, value, true);
    //            }
    //        }
    //    }

    //    public void CopyTo(jsObject Target) {
    //        foreach (var Pair in this){
    //            Target[Pair.Key] = Pair.Value;
    //        }
    //    }

    //    public void ForEach(Action<string, object> Action) {
    //        this.AsParallel().ForAll((Pair) =>{
    //            Action(Pair.Key, Pair.Value);
    //        });
    //    }

    //    public T getObject<T>(params string[] Key) {
    //        object val = get(Key);

    //        if (val is T) {
    //            return (T)val;
    //        } else if (val is string) {
    //            try {
    //                var Method = typeof(T).GetMethod("Parse", ParseTypes);

    //                if (Method != null) {
    //                    return (T)Method.Invoke(null, new object[] { val });
    //                }
    //            } 
    //            catch { }
    //        } else {
    //            try {
    //                return (T)val;
    //            }
    //            catch { }
    //        }

    //        return default(T);
    //    }

    //    public T Assert<T>(params string[] Key) {
    //        object obj = getObject<T>(Key);

    //        if (obj == null)
    //            throw new Exception("Assertion Failed!");

    //        return (T)obj;
    //    }

    //    public T Options<T>(params string[] Key) {
    //        int Index = 0;

    //        while (Index < Key.Length) {
    //            object obj = getObject<T>(Key[Index]);

    //            if (obj != null)
    //                return (T) obj;
    //        }

    //        return default(T);
    //    }

    //    public T Search<T>(string Key, bool IgnoreCase = true) {
    //        foreach (var Pair in this) {
    //            if (Pair.Value is T) {
    //                if (Key.Compare(Pair.Key, IgnoreCase)) {
    //                    return (T)Pair.Value;
    //                }
    //            }
    //        }

    //        return default(T);
    //    }

    //    public int getInt(params string[] Key) {
    //        return getObject<int>(Key);
    //    }

    //    public double getDouble(params string[] Key) {
    //        return getObject<double>(Key);
    //    }

    //    public bool getBool(params string[] Key) {
    //        return getObject<bool>(Key);
    //    }

    //    public string getString(params string[] Key) {
    //        return getObject<string>(Key);
    //    }

    //    public jsObject getObject(params string[] Key) {
    //        return getObject<jsObject>(Key);
    //    }

    //    private bool _Raw(string Text, ref int Index, params string[] Key) {
    //        var Token = Text.ComesFirst(",", "}", "]");
    //        var SubIndex = Text.IndexOf(Token, Index);

    //        if (SubIndex == -1)
    //            SubIndex = Text.Length;

    //        var Sub = Text.Substring(Index, SubIndex - Index);

    //        this[Key] = getObjectValue(Sub.Trim());

    //        Index += Sub.Length;

    //        return true;
    //    }

    //    private bool _String(string Text, ref int Index, params string[] Key) {
    //        if (Text[Index] == '"') {
    //            var Sub = Text.Substring("\"", "\"", Index, true);

    //            Index += Sub.Length;

    //            this[Key] = Sub.Substring(1, Sub.Length - 2).Descape();
    //        }
    //        else if (Text[Index] == '\'') {
    //            var Sub = Text.Substring("'", "'", Index, true);

    //            Index += Sub.Length;

    //            this[Key] = Sub.Substring(1, Sub.Length - 2).Descape();
    //        }
    //        else {
    //            return false;
    //        }

    //        return true;
    //    }

    //    private bool _Array(string Text, ref int Index, params string[] Key) {
    //        var SubIndex = 0;
    //        var This = (jsObject)null;

    //        if (Key.Length == 0) {
    //            This = this;
    //        }
    //        else {
    //            This = new jsObject();
    //            this[Key] = This;
    //        }

    //        This.IsArray = true;

    //        Text = Text.FindMatchingBrackets("[", "]", Index, true);

    //        Index += Text.Length;

    //        Text = Text.Substring(1, Text.Length - 2);

    //        do {
    //            if (Text[SubIndex] == ',') {
    //                SubIndex++;
    //            }

    //            while (SubIndex < Text.Length && char.IsWhiteSpace(Text[SubIndex]))
    //                SubIndex++;

    //            if (SubIndex == Text.Length)
    //                break;


    //            var X = Text[SubIndex];
    //            var Name = This.Count.ToString();

    //            if (X == '"') {
    //                if (!This._String(Text, ref SubIndex, Name))
    //                    return false;
    //            }
    //            else if (X == '\'') {
    //                if (!This._String(Text, ref SubIndex, Name))
    //                    return false;
    //            }
    //            else if (X == '{') {
    //                if (!This._Object(Text, ref SubIndex, Name))
    //                    return false;
    //            }
    //            else if (X == '[') {
    //                if (!This._Array(Text, ref SubIndex, Name))
    //                    return false;
    //            }
    //            else {
    //                if (!This._Raw(Text, ref SubIndex, Name))
    //                    return false;
    //            }
    //        }
    //        while (SubIndex < Text.Length);

    //        return true;
    //    }

    //    private bool _Object(string Text, ref int Index, params string[] Key) {
    //        var SubIndex = 0;
    //        var This = (jsObject)null;

    //        if (Key.Length == 0) {
    //            This = this;
    //        }
    //        else {
    //            This = new jsObject();
    //            this[Key] = This;
    //        }

    //        This.IsObject = true;

    //        Text = Text.FindMatchingBrackets("{", "}", Index, true);

    //        Index += Text.Length;

    //        Text = Text.Substring(1, Text.Length - 2);

    //        do {
    //            if (Text[SubIndex] == ',') {
    //                SubIndex++;
    //            }

    //            while (SubIndex < Text.Length && char.IsWhiteSpace(Text[SubIndex]))
    //                SubIndex++;

    //            if (SubIndex == Text.Length)
    //                break;

    //            if (!This._NamedValue(Text, ref SubIndex)) {
    //                return false;
    //            }
    //        }
    //        while (SubIndex < Text.Length);

    //        return true;
    //    }

    //    private bool _NamedValue(string Text, ref int Index) {
    //        var Name = "";
    //        var X = Text[Index];

    //        if (X == '"') {
    //            Name = Text.FindMatchingBrackets("\"", "\"", Index);

    //            Index += Name.Length + 2;
    //        }
    //        else if (X == '\'') {
    //            Name = Text.FindMatchingBrackets("'", "'", Index);

    //            Index += Name.Length + 2;
    //        }
    //        else {
    //            Name = Text.Substring("", ":", Index, false).Trim();
    //        }

    //        Index = Text.IndexOf(':', Index);

    //        if (Index == -1)
    //            return false;
    //        else {
    //            Index++;

    //            while (char.IsWhiteSpace(Text[Index]))
    //                Index++;
    //        }


    //        X = Text[Index];

    //        if (X == '"') {
    //            return _String(Text, ref Index, Name);
    //        }
    //        else if (X == '\'') {
    //            return _String(Text, ref Index, Name);
    //        }
    //        else if (X == '{') {
    //            return _Object(Text, ref Index, Name);
    //        }
    //        else if (X == '[') {
    //            return _Array(Text, ref Index, Name);
    //        }
    //        else {
    //            return _Raw(Text, ref Index, Name);
    //        }
    //    }

    //    public bool Parse(string Text) {
    //        int Index = 0;
            
    //        if (Text == null) {
    //            return false;
    //        } else {
    //            Text = Text.Trim();
    //        }
    //        if (Text == string.Empty) {
    //            return true;
    //        }

    //        if (Text.StartsWith("{") && Text.EndsWith("}")) {
    //            return _Object(Text, ref Index);
    //        }
    //        else if (Text.StartsWith("[") && Text.EndsWith("]")) {
    //            return _Array(Text, ref Index);
    //        }
    //        else {
    //            return _Object("{ " + Text + " }", ref Index);
    //        }
    //    }

    //    public override string ToString() {
    //        return ToString(jsObject.HumanFormatting);
    //    }

    //    public virtual string ToString(bool HumanFormat = false) {
    //        return jsObject.Stringify(this, HumanFormat);
    //    }

    //    public static string Stringify(jsObject This, bool HumanFormat, int Reserved = 1) {
    //        string CurTab = new string('\t', Reserved);
    //        StringBuilder Output = new StringBuilder();

    //        if (This.IsObject){
    //            Output.Append("{");
    //        }
    //        else if (This.IsArray) {
    //            Output.Append("[");
    //        }

    //        if (HumanFormat) {
    //            Output.AppendLine().Append(CurTab);
    //        }

    //        foreach (KeyValuePair<string, object> Pair in This) {
    //            if (!Pair.Key.IsNumeric()) {
    //                Output.AppendFormat("'{0}':", Pair.Key);
    //            }

    //            if (Pair.Value is jsObject) {
    //                Output.Append(
    //                    jsObject.Stringify((Pair.Value as jsObject), HumanFormat, Reserved + 1)
    //                );
    //            } else if (Pair.Value is string) {
    //                Output.AppendFormat("'{0}'", Pair.Value.ToString().Escape());
    //            } else {
    //                Output.Append(Pair.Value);
    //            }

    //            if (This.Count > 1 && Pair.Key != This.Last().Key) {
    //                Output.Append(',');
    //            } else {
    //                CurTab = CurTab.Substring(0, CurTab.Length - 1);
    //            }

    //            if (HumanFormat) {
    //                Output.Append(Environment.NewLine).Append(CurTab);
    //            }
    //        }

    //        if (This.IsObject) {
    //            Output.Append("}");
    //        }
    //        else if (This.IsArray) {
    //            Output.Append("]");
    //        }


    //        return Output.ToString();
    //    }

    //    public static jsFile FromFile(string fileName) {
    //        jsFile Object = new jsFile();

    //        if (Object.Load(fileName)) {
    //            return Object;
    //        }

    //        return default(jsFile);
    //    }

    //    public static jsObject FromUrl(string Url, string Data = "") {
    //        System.Net.WebClient Client = new System.Net.WebClient();
    //        if (string.IsNullOrEmpty(Data)) {
    //            Data = Client.DownloadString(Url);
    //        } else {
    //            Data = Client.UploadString(Url, Data);
    //        }

    //        jsObject Object = new jsObject();

    //        if (Object.Parse(Data)) {
    //            return Object;
    //        }

    //        return default(jsObject);
    //    }

    //    public static jsObject FromEmbeded(string Name) {
    //        Data.jsObject Obj = "";
    //        var asm = Assembly.GetCallingAssembly();

    //        try {
    //            using (StreamReader Reader = new StreamReader(asm.GetManifestResourceStream(Name))) {
    //                Obj = Reader.ReadToEnd();
    //            }
    //        } catch {
    //            return "";
    //        }

    //        return Obj;
    //    }

    //    public static implicit operator jsObject(string Text) {
    //        jsObject Object = new jsObject();

    //        if (Object.Parse(Text)) {
    //            return Object;
    //        }

    //        return default(jsObject);
    //    }
    //}

    //public class jsObject<T> : jsObject {
    //    public new T this[params string[] Key] {
    //        get {
    //            return (T)base[Key];
    //        }
    //        set {
    //            base[Key] = value;
    //        }
    //    }
    //}
}