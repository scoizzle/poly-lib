using System;
using System.Security.Cryptography;

namespace Poly.Data {
	public class Hash {
		private static string getHash(HashAlgorithm alg, byte[] toHash) {
			string szHash = string.Empty;
			byte[] btHash = alg.ComputeHash(toHash);
			
			for (int Index=0;Index<btHash.Length;Index++){
                szHash += btHash[Index].ToString("x2");
			}
			
			return szHash;
		}
		
		public static string MD5(byte[] toHash) {
                return getHash(HashAlgorithm.Create("MD5"), toHash);
		}

        public static string SHA256(byte[] toHash) {
                return getHash(HashAlgorithm.Create("SHA256"), toHash);
		}

        public static string SHA512(byte[] toHash) {
                return getHash(HashAlgorithm.Create("SHA512"), toHash);
		}
	}
}

