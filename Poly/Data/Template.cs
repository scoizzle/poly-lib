﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Poly.Data {
    public static class jsObjectExtension {
        public static string Template(this jsObject This, string Template) {
            string Output = Template;

            if (This == null)
                return string.Empty;

            foreach (var Pair in This.ToList()) {
                var Open = string.Join("", "{", Pair.Key, "}");
                var Close = string.Join("", "{/", Pair.Key, "}");

                if (!Output.Contains(Open))
                    continue;

                if (Pair.Value is jsObject) {
                    var Section = Template.FindMatchingBrackets(Open, Close);

                    if (string.IsNullOrEmpty(Section))
                        continue;

                    var SubOutput = new StringBuilder();

                    if ((Pair.Value as jsObject).IsArray) {
                        foreach (var subPair in (Pair.Value as jsObject)) {
                            SubOutput.Append(
                                (subPair.Value as jsObject).Template(Section)
                            );
                        }
                    }
                    else {
                        SubOutput.Append(
                            (Pair.Value as jsObject).Template(Section)
                        );
                    }

                    Output = Output.Replace(Open + Section + Close, SubOutput.ToString());
                } 
                else {
                    Output = Output.Replace(Open, This.getString(Pair.Key));
                }
            }

            return Output;
        }
    }
}
