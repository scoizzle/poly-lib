﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

using Poly.Data;

namespace Poly.Net.Tcp {
    public class Packet : jsObject {
        public string Raw {
            get {
                if (ContainsKey("Raw")) {
                    return getString("Raw");
                }
                return "";
            }
            set {
                this["Raw"] = value;
            }
        }

        public virtual bool Send(Client Client) {
            if (!Client.Connected)
                return false;

            Client.Writer.WriteLine(Raw);
            return true;
        }

        public virtual bool Receive(Client Client) {
            if (!Client.Connected)
                return false;

            Raw = Client.Reader.ReadLine();
            return true;
        }

        public static implicit operator Packet(string Input) {
            return new Packet() { 
                Raw = Input 
            };
        }

        public override string ToString() {
            return Raw.ToString();
        }
    }
}
