﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;


namespace Poly.Net.Tcp {
    public class Server : TcpListener {
        public int Port = int.MaxValue;

        public delegate void ClientConnectHandler(Client Client);
        public event ClientConnectHandler ClientConnect;

        public new bool Active {
            get {
                return base.Active;
            }
        }

        public Server(int port)
            : this(IPAddress.Any, port) {
            this.Port = port;
        }

        public Server(IPAddress addr, int port)
            : base(addr, port) {
            this.Port = port;
        }

        public new void Start() {
            base.Start();

            this.BeginAcceptTcpClient(new AsyncCallback(OnClientConnect), this);
        }

        public new void Stop() {
            base.Stop();
        }

        private void OnClientConnect(IAsyncResult Result) {
            ThreadPool.QueueUserWorkItem(
                new WaitCallback((State) => {
                    OnConnect((State as TcpClient).Client);
                }),
                this.EndAcceptTcpClient(Result)
            );

            if (Active)
                this.BeginAcceptTcpClient(new AsyncCallback(OnClientConnect), this);
        }

        public virtual void OnConnect(Socket con) {
            if (this.ClientConnect != null) {
                this.ClientConnect((Client)con);
            }
        }
    }
}
