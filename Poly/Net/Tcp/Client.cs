using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Poly.Net.Tcp {
	public class Client : TcpClient {
        private StreamReader p_sRead = null;
        private StreamWriter p_sWrite = null;
        private bool p_bAutoFlush = true;

        public bool autoFlush {
            get {
                if (Writer == null)
                    return p_bAutoFlush;
                if (p_bAutoFlush != Writer.AutoFlush)
                    Writer.AutoFlush = p_bAutoFlush;
                return p_bAutoFlush;                
            }
            set {
                p_bAutoFlush = value;
                if (Writer != null) {
                    Writer.AutoFlush = value;
                }
            }
        }
		
        public Client()
            : base() {
        }

        public Client(Socket Base) {
            this.Client = Base;
        }

        public StreamReader Reader {
            get {
                if (p_sRead == null) {
                    p_sRead = new StreamReader(this.GetStream());
                }
                return p_sRead;
            }
        }

        public StreamWriter Writer {
            get {
                if (p_sWrite == null) {
                    p_sWrite = new StreamWriter(this.GetStream());
                    p_sWrite.AutoFlush = p_bAutoFlush;
                }
                return p_sWrite;
            }
        }

		public void Reconnect () {
			if (Connected)
				Close ();

            var Remote = Client.RemoteEndPoint.ToString().Split(':');
            string IP = Remote[0], Port = Remote[1];

			Client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			Connect(IP, int.Parse(Port));
		}

        new public void Close () {
            try {
                if (Client.Connected) {
                    Client.Shutdown(SocketShutdown.Both);
                    p_sRead = null;
                    p_sWrite = null;
                }
                Client.Close(0);
            }
            catch { }
		}

        public bool Send(Packet Packet) {
            return Packet.Send(this);
        }

        public Packet Recieve() {
            Packet Packet = new Packet();

            if (Packet.Receive(this)) {
                return Packet;
            }

            return string.Empty;
        }

        public Packet Request(string Data) {
            Send(Data);
            return Recieve();
        }


        public static implicit operator Client(Socket Sock) {
            return new Client(Sock);
        }
	}
}

