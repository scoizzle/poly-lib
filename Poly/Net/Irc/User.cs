﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poly.Net.Irc {
    public class User : Data.jsObject {
        public User() {
            Nick = string.Empty;
            Ident = string.Empty;
            Host = string.Empty;
        }

        public User(string Raw = "") {
            if (!Raw.Contains("!")) {
                Nick = Raw;
            } else {
                string[] Data = Raw.Split('!', '@');

                Nick = Data[0];
                Ident = Data[1];
                Host = Data[2];
			}
        }

		public string Nick {
			get {
				return getString("Nick");
			}
			set {
				this["Nick"] = value;
			}
		}

		public string Ident {
			get {
				return getString("Ident");
			}
			set {
				this["Ident"] = value;
			}
		}

		public string Host {
			get {
				return getString("Host");
			}
			set {
				this["Host"] = value;
			}
		}

		public string Mode(string channel) {
            return getString("Mode", channel);
		}

		public string Mode(string channel, string value) {
			if (value.StartsWith("+")) {
                this["Mode", channel] = Mode(channel) + value.Substring(1);
			} else if (value.StartsWith("-")) {
				foreach (char mod in value) {
					this["Mode", channel] = Mode(channel).Replace(mod.ToString(), "");
				}
			} else {
				this["Mode", channel] = value;
			}
			return Mode(channel);
		}
    }
}
