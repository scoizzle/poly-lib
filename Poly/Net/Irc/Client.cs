﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Poly.Net.Irc {
    public partial class Client : User {
        private Tcp.Client Connection = new Tcp.Client();
        private System.Threading.Thread ConnectionHandlerThread = null;

		public Data.jsObject Conversations {
			get {
                if (!this.ContainsKey("Conversations")) {
                    this["Conversations"] = new Data.jsObject();
                }
                return getObject("Conversations");
			}
		}

        public Data.jsObject Users {
            get {
                if (!this.ContainsKey("Users")) {
                    this["Users"] = new Data.jsObject();
                }
                return getObject("Users");
            }
        }

        public Client() :  base() {
            Username = string.Empty;
            Password = string.Empty;
            Server = string.Empty;
            Port = 0;
        }

        public string Server {
            get {
                return getString("Server");
            }
            set {
                this["Server"] = value;
            }
        }

        public int Port {
            get {
                int port = getInt("Port");

                if (port != default(int))
                    return port;

                return 6667;
            }
            set {
                this["Port"] = value;
            }
        }

        public string Username {
            get {
				return getString("Username");
            }
            set {
                this["Username"] = value;
            }
        }

        public string Password {
            get {
				return getString("Password");
            }
            set {
                this["Password"] = value;
            }
        }

        public string Realname {
            get {
                return getString("Realname");
            }
            set {
                this["Realname"] = value;
            }
        }

        public bool Connected {
            get {
                return (this.Connection == null) ? false : this.Connection.Connected;
            }
            set {
                if (value) {
                    this.Start();
                } else {
                    this.Stop();
                }
            }
        }

        public override string ToString() {
            return this.ToString(true);
        }

        public override string ToString(bool humanFormat = false) {
            Data.jsObject Object = new Data.jsObject();

            foreach (string Key in this.Keys) {
                switch (Key) {
                    default:
                    Object[Key] = this[Key];
                    break;

                    case "Conversations": {
                        int Index = 0;
                        foreach (Conversation Convo in this.Conversations.Values) {
                            if (Convo.Name.StartsWith("#")) {
                                Object["Conversations", Index.ToString()] = Convo.Name;
                                Index++;
                            }
                        }
                    } break;

                    case "Mode":
                    case "Users":
                    break;
                }
            }

            return Object.ToString(humanFormat);
        }
    }
}
