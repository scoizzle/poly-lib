﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poly.Net.Irc {
    public class Packet : Net.Tcp.Packet {
        public Packet() {
            ID = ID;
            Raw = "";
            Sender = "";
            Receiver = "";
            Action = "";
            Message = "";
        }

        public int ID {
            get {
                int Id = getInt("ID");

                if (Id != default(int))
                    return Id;

                return -1;
            }
            set {
                this["ID"] = value;
            }
        }

        public string Sender {
            get {
                return getString("Sender");
            }
            set {
                this["Sender"] = value;
            }
        }

        public string Receiver {
            get {
                return getString("Reciever");
            }
            set {
                this["Reciever"] = value;
            }
        }

        public string Action {
            get {
                return getString("Action");
            }
            set {
                this["Action"] = value;
            }
        }

        public string Message {
            get {
                return getString("Message");
            }
            set {
                this["Message"] = value;
            }
        }

        public override bool Receive(Tcp.Client Client) {
            if (!base.Receive(Client))
                return false;

            if (string.IsNullOrEmpty(Raw))
                return false;

            try {
                string[] subStrings = Raw.Split(' ');
                if (subStrings[0].StartsWith(":")) {
                    do {
                        Sender = subStrings[0].Substring(1);
                        if (subStrings[1].IsNumeric()) {
                            ID = int.Parse(subStrings[1]);
                        }
                        else {
                            Action = subStrings[1];
                        }
                        if (subStrings.Length > 2 && !subStrings[2].StartsWith(":")) {
                            Receiver = subStrings[2];
                        }
                        else {
                            Message = subStrings[2];
                        }

                        if (subStrings.Length < 4)
                            break;

                        if (subStrings[3].StartsWith("=")) {
                            Receiver = subStrings[4];
                            Message = string.Join(" ", subStrings, 5, subStrings.Length - 5);
                        }
                        else if (subStrings[3].StartsWith("#")) {
                            Receiver = subStrings[3];

                            Message = Raw.Substring(
                                Raw.IndexOf(Receiver) + Receiver.Length + 1
                                );
                        }
                        if (subStrings[3] == "@") {
                            Receiver = subStrings[4];
                            Message = string.Join(" ", subStrings, 5, subStrings.Length - 5);
                        }
                        else {
                            Message = string.Join(" ", subStrings, 3, subStrings.Length - 3);
                        }
                    } while (false);
                }
                else {
                    Action = subStrings[0];
                    Message = subStrings[1].StartsWith(":") ? subStrings[1].Substring(1) : subStrings[1];
                }

                if (Message.StartsWith(":")) {
                    Message = Message.Substring(1);
                    if (Message.StartsWith("\x01")) {
                        Action = Message.Substring(1, Message.IndexOf("\x01", 1) - 1);
                        if (Action.Contains(" ")) {
                            Message = Action.Substring(Action.IndexOf(' ') + 1);
                            Action = Action.Substring(0, Action.IndexOf(' '));
                        }
                    }
                    Message = Message.TrimEnd();
                }
            }
            catch (Exception Error) {
                Console.WriteLine("Error Parsing Packet: ", Raw);
                Console.WriteLine("Error: {0}", Error.ToString());
                return false;
            }

            return true;
        }

        public static implicit operator Packet(string Input) {
            return new Packet() {
                Raw = Input
            };
        }

        public static implicit operator String(Packet Input) {
            return Input.Raw;
        }

        public static readonly Packet Pass = "PASS {0}";
        public static readonly Packet User = "USER {0} {1} * :{2}";
        public static readonly Packet Nick = "NICK {0}";
        public static readonly Packet Pong = "PONG :{0}";
        public static readonly Packet Quit = "QUIT :{0}";

        public static readonly Packet Join = "JOIN {0}";
        public static readonly Packet Part = "PART {0} :{1}";
        public static readonly Packet Topic = "TOPIC {0} :{1}";
        public static readonly Packet Invite = "INVITE {0} {1}";
        public static readonly Packet Notice = "NOTICE {0} :{1}";
        public static readonly Packet SendMsg = "PRIVMSG {0} :{1}";
        public static readonly Packet ModeSingle = "MODE {0} {1}";
        public static readonly Packet ModeDouble = "MODE {0} {1} {2}";
        public static readonly Packet Kick = "KICK {0} {1} :{2}";

        public static readonly Packet LUsers = "LUSERS";
        public static readonly Packet MOTD = "MOTD";
        public static readonly Packet Version = "VERSION";
        public static readonly Packet Stats = "STATS {0}";
        public static readonly Packet Links = "LINKS";
        public static readonly Packet Time = "TIME";
        public static readonly Packet Trace = "TRACE";
        public static readonly Packet Admin = "ADMIN";
        public static readonly Packet Info = "INFO";
        public static readonly Packet Oper = "OPER {0} {1}";
        public static readonly Packet Names = "NAMES {0}";
        public static readonly Packet List = "LIST {0}";
        public static readonly Packet Who = "WHO {0}";
        public static readonly Packet Whois = "WHOIS {0}";
        public static readonly Packet Whowas = "WHOWAS {0}";
    }
}
