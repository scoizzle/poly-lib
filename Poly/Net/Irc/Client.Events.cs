﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Poly.Net.Irc {
    public partial class Client : User {
        public delegate void OnString(string Data);
        public delegate void OnPacket(Packet Data);
        public delegate void OnHandler(string Data, params object[] Args);
        public delegate void OnUserString(User User, string Message);
        public delegate void OnConvoUser(Conversation Convo, User User);
        public delegate void OnConvoUserString(Conversation Convo, User User, string Message);

        public Hashtable EventTable = new Hashtable();

        public object[] InvokeEvent(string EventName, params object[] args) {
            List<object> returnValues = new List<object>();
            if (EventTable.ContainsKey(EventName)) {
                foreach (var Function in ((List<object>)EventTable[EventName])) {
                    returnValues.Add((Function as Delegate).DynamicInvoke(args));
                }
                if (EventTable.ContainsKey("OnEvent")){
                    foreach (var Function in ((List<object>)EventTable["OnEvent"])) {
                        (Function as Delegate).DynamicInvoke(EventName, args);
                    }
                }
            }
			Application.Log.Info("IRC_CLIENT_EVENTS INVOKE( " + EventName + " )");
            return returnValues.ToArray();
        }

        public void AddEvent(string EventName, object Function) {
            if (EventTable[EventName] == null) {
                EventTable[EventName] = new List<object>();
            }
            Application.Log.Info("IRC_CLIENT_EVENTS ADD( " + EventName + " )");
            ((List<object>)EventTable[EventName]).Add(Function);
        }

        public void RemoveEvent(string EventName, object Function) {
            if (EventTable[EventName] == null) {
                return;
            }
            Application.Log.Info("IRC_CLIENT_EVENTS REMOVE( " + EventName + " )");
            ((List<object>)EventTable[EventName]).Remove(Function);
        }

        public event OnHandler OnEvent {
            add {
                AddEvent("OnEvent", value);
            }
            remove {
                RemoveEvent("OnEvent", value);
            }
        }

        public event OnPacket OnData {
            add {
                AddEvent("OnData", value);
            }
            remove {
                RemoveEvent("OnData", value);
            }
        }

        public event OnString OnConnect {
            add {
                AddEvent("OnConnect", value);
            }
            remove {
                RemoveEvent("OnConnect", value);
            }
        }

        public event OnString OnDisconnect {
            add {
                AddEvent("OnDisconnect", value);
            }
            remove {
                RemoveEvent("OnDisconnect", value);
            }
        }

        public event OnString OnPing {
            add {
                AddEvent("OnPing", value);
            }
            remove {
                RemoveEvent("OnPing", value);
            }
        }

        public event OnUserString OnUserNick {
            add {
                AddEvent("OnUserNick", value);
            }
            remove {
                AddEvent("OnUserNick", value);
            }
        }

        public event OnUserString OnUserQuit {
            add {
                AddEvent("OnUserQuit", value);
            }
            remove {
                AddEvent("OnUserQuit", value);
            }
        }

        public event OnConvoUser OnUserJoin {
            add {
                AddEvent("OnUserJoin", value);
            }
            remove {
                RemoveEvent("OnUserJoin", value);
            }
        }

        public event OnConvoUser OnUserPart {
            add {
                AddEvent("OnUserPart", value);
            }
            remove {
                RemoveEvent("OnUserPart", value);
            }
        }

        public event OnConvoUserString OnChannelTopic {
            add {
                AddEvent("OnChannelTopic", value);
            }
            remove {
                RemoveEvent("OnChannelTopic", value);
            }
        }

        public event OnConvoUserString OnChannelMessage {
            add {
                AddEvent("OnChannelMessage", value);
            }
            remove {
                RemoveEvent("OnChannelMessage", value);
            }
        }

        public event OnConvoUserString OnChannelNotice {
            add {
                AddEvent("OnChannelNotice", value);
            }
            remove {
                RemoveEvent("OnChannelNotice", value);
            }
        }

        public event OnConvoUserString OnChannelMode {
            add {
                AddEvent("OnChannelMode", value);
            }
            remove {
                RemoveEvent("OnChannelMode", value);
            }
        }

        public event OnConvoUser OnChannelUserList {
            add {
                AddEvent("OnChannelUserList", value);
            }
            remove {
                RemoveEvent("OnChannelUserList", value);
            }
        }

        public event OnConvoUserString OnUserMessage {
            add {
                AddEvent("OnUserMessage", value);
            }
            remove {
                RemoveEvent("OnUserMessage", value);
            }
        }

        public event OnConvoUserString OnUserNotice {
            add {
                AddEvent("OnUserNotice", value);
            }
            remove {
                AddEvent("OnUserNotice", value);
            }
        }

        public event OnConvoUserString OnUserAction {
            add {
                AddEvent("OnUserAction", value);
            }
            remove {
                RemoveEvent("OnUserAction", value);
            }
        }

        public event OnConvoUserString OnUserKick {
            add {
                AddEvent("OnUserKick", value);
            }
            remove {
                RemoveEvent("OnUserKick", value);
            }
        }

        public event OnConvoUserString OnUserMode {
            add {
                AddEvent("OnUserMode", value);
            }
            remove {
                RemoveEvent("OnUserMode", value);
            }
        }
    }
}
