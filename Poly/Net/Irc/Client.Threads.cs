﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Poly.Net.Irc {
    public partial class Client : User {
        public void HandleBasicConnection() {
            this.RegisterConnection();

            var Packet = (Packet)null;

            do {
                if ((Packet = new Packet()).Receive(Connection)) {
                    (new Thread(HandleBasicPacket)).Start(Packet);
                }
            } while (Connection.Connected);

            Application.Log.Warning("IRC_CLIENT Connection lost.");
            Stop();
            Application.Log.Warning("IRC_CLIENT Reconnecting.");
            Start();
        }

        public void HandleBasicPacket(object raw) {
            if (raw == null)
                return;
            try {
                Packet Packet = (Packet)raw;
                Conversation Convo = null;
                User Sender = null;

                if (Packet.Receiver == null)
                    return;

                string SenderNick = string.Empty;

                if (Packet.Sender.Contains("!")) {
                    SenderNick = Packet.Sender.Substring("", "!");
                }
                else {
                    SenderNick = Packet.Sender;
                }

                if (Packet.Receiver.Equals(this.Nick)) {
                    if (Conversations.ContainsKey(SenderNick)) {
                        Convo = Conversations.getObject<Conversation>(SenderNick);
                    }
                    else {
                        Convo = new Conversation(SenderNick);
                        Conversations[SenderNick] = Convo;
                    }
                }
                else if (Conversations.ContainsKey(Packet.Receiver)) {
                    Convo = Conversations.getObject<Conversation>(Packet.Receiver);
                }
                else {
                    Convo = new Conversation(Packet.Receiver);
                    Conversations[Packet.Receiver] = Convo;
                }

                if (Users.ContainsKey(SenderNick)) {
                    Sender = Users.getObject<User>(SenderNick);
                }
                else {
                    Sender = new User(Packet.Sender);
                    Users[SenderNick] = Sender;
                }

                switch (Packet.Action) {
                    default:
                        break;
                    case "PING": {
                            if (string.IsNullOrEmpty(Packet.Sender)) {
                                SendPong(Packet.Message);
                            }
                            else {
                                SendCTCPReply(Packet.Sender, "PING " + Packet.Message);
                            }
                        }
                        break;

                    case "PRIVMSG": {
                            if (this.Nick.Equals(Convo.Name)) {
                                this.InvokeEvent("OnUserMessage", Convo, Sender, Packet.Message);
                            }
                            else {
                                this.InvokeEvent("OnChannelMessage", Convo, Sender, Packet.Message);
                            }
                        }
                        break;

                    case "NOTICE": {
                            if (this.Nick.Equals(Convo.Name)) {
                                this.InvokeEvent("OnUserNotice", Convo, Sender, Packet.Message);
                            }
                            else {
                                this.InvokeEvent("OnChannelNotice", Convo, Sender, Packet.Message);
                            }
                        }
                        break;

                    case "ACTION": {
                            this.InvokeEvent("OnUserAction", Convo, Sender, Packet.Message);
                        }
                        break;

                    case "JOIN": {
                            if (Sender.Nick == this.Nick)
                                break;
                            if (Conversations.ContainsKey(Packet.Message)) {
                                Convo = Conversations.getObject<Conversation>(Packet.Message);
                            }
                            else {
                                Convo = new Conversation(Packet.Message);
                            }
                            this.InvokeEvent("OnUserJoin", Convo, Sender);
                        }
                        break;

                    case "PART": {
                            if (Convo.Users.ContainsKey(Sender.Nick)) {
                                Convo.Remove(Sender.Nick);
                            }
                            this.InvokeEvent("OnUserPart", Convo, Sender);
                        }
                        break;

                    case "MODE": {
                            var Action = Packet.Message[0].ToString();
                            var Targets = new string[0];

                            if (Packet.Message.Contains(" ")) {
                                Targets = Packet.Message.Substring(" ").Split(',');
                            }
                            else {
                                Targets = Packet.Message.Split(',');
                            }

                            if (Packet.Receiver.Equals(this.Nick)) {
                                this.Mode("", Packet.Message);
                                this.InvokeEvent("OnUserMode", Convo, Sender, this);
                            }
                            else {
                                for (int X = 1; Packet.Message[X] != ' ' && X - 1 < Targets.Length; X++) {
                                    var Mode = Action + Packet.Message[X];
                                    var Targ = Targets[X - 1];

                                    if (Users.ContainsKey(Targ)) {
                                        Users.getObject<User>(Targ).Mode(Convo.Name, Mode);
                                        this.InvokeEvent("OnUserMode", Convo, Sender, Mode);
                                    }
                                    else if (Targ.StartsWith("#") && Conversations.ContainsKey(Targ)) {
                                        Conversations.getObject<Conversation>(Targ).Mode = Mode;
                                        this.InvokeEvent("OnChannelMode", Convo, Sender, Mode);
                                    }
                                }
                            }
                        }
                        break;

                    case "NICK": {
                            if (!Convo.Users.ContainsKey(Sender.Nick)) {
                                Convo.Users.Add(Sender.Nick, Sender);
                            }
                            this.InvokeEvent("OnUserNick", Sender, Packet.Receiver.Trim());
                            Sender.Nick = Packet.Receiver.Trim();
                        }
                        break;

                    case "KICK": {
                            Convo.Users.Remove(Sender.Nick);
                            this.InvokeEvent("OnUserKick", Convo, Sender, Packet.Message.Trim());
                        }
                        break;

                    case "QUIT": {
                            foreach (var Name in this.Conversations.Keys) {
                                var Chat = Conversations.getObject<Conversation>(Name);

                                if (Chat == null || Chat.Users == null)
                                    continue;

                                if (Chat.Users.ContainsKey(Sender.Nick))
                                    Chat.Users.Remove(Sender.Nick);

                                if (Users.ContainsKey(Sender.Nick))
                                    Users.Remove(Sender.Nick);
                            }
                            this.InvokeEvent("OnUserQuit", Sender, Packet.Message.Trim());
                        }
                        break;
                }

                switch (Packet.ID) {
                    default:
                        break;
                    case 1: {
                            this.InvokeEvent("OnConnect", Packet.Message.Trim());
                        }
                        break;

                    case 332: {
                            Convo.Topic = Packet.Message;
                            this.InvokeEvent("OnChannelTopic", Convo, Sender, Packet.Message);
                        }
                        break;

                    case 352: {

                            string[] Split = Packet.Message.Split(' ');

                            if (!Conversations.ContainsKey(Split[0])) {
                                Conversations.Add(Split[0], new Conversation(Split[0]));
                            }
                            Sender = new User(Split[4]);
                            if (!Convo.Users.ContainsKey(Split[4])) {
                                Convo.Users.Add(Split[4], Sender);
                            }
                            else {
                                Sender = Convo.getObject<User>("Users", Split[4]);
                            }
                            if (!this.Users.ContainsKey(Split[4])) {
                                this.Users.Add(Split[4], Sender);
                            }
                            Sender.Ident = Split[1];
                            Sender.Host = Split[2];
                            Sender.Mode(Split[0], FixModes(Split[5]));

                            this.InvokeEvent("OnChannelUserList", Convo, Sender);
                        }
                        break;

                    case 366: {
                            SendWho(
                                Packet.Message.Substring(
                                    "", " "
                                )
                            );
                        }
                        break;

                    case 396: {
                            this.Host = Packet.Message.Substring(
                                "", " :"
                            );
                        }
                        break;

                    case 433: {
                            this.Nick += "_";
                            this.SendNick(this.Nick);
                        }
                        break;
                }

                this.InvokeEvent("OnData", Packet);
            }
            catch (Exception Error) {
                Application.Log.Error("IRC_CLIENT_HANDLE_PACKET " + Error.ToString());
            }
        }
    }
}