﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Poly.Net.Irc {
    public partial class Client : User {
        public void Connect() {
            Connect(this.Server, this.Port);
        }

        public void Connect(string host, int port = 6667) {
            if (Connection == null) {
                Connection = new Tcp.Client();
            }

            try {
                Connection.Connect(host, port);
            }
            catch (Exception Error) {
                Application.Log.Error(Error.ToString());
            }

            Connection.autoFlush = true;
        }

        public void Disconnect() {
            if (Connection != null) {
                Connection.Close();
            }
        }

        public void Start() {
            if (this.Connection == null || !this.Connection.Connected)
                Connect();

            this.ConnectionHandlerThread = new System.Threading.Thread(HandleBasicConnection);
            this.ConnectionHandlerThread.Start();
        }

        public void Stop() {
            if (this.Connection != null) {
                this.Connection.Close();
            }
            this.ConnectionHandlerThread = null;
            this.Connection = null;
        }

        public void Send(string Packet, params object[] Args) {
            if (Args.Length > 0) {
                Packet = string.Format(Packet, Args);
            }

            if (Packet.Contains("\n")) {
                string Header = Packet.Substring("", ":", 1);
                string[] Lines = Packet.Substring(":", "", 1).Split('\n');

                foreach (string Msg in Lines) {
                    Connection.Send(Header + Msg);
                }
            }
            else {
                if (Connection != null) {
                    Connection.Send(Packet);
                }
            }
        }

        public void SendCTCP(string Target, string Data) {
            SendMessage(
                Target,
                string.Format("\x0001{0}\x0001", Data)
            );
        }

        public void SendCTCPReply(string Target, string Data) {
            SendNotice(
                Target,
                string.Format("\x0001{0}\x0001", Data)
            );
        }

        public void SendUser(string Name, string RealName, bool Hidden = false) {
            Send(Packet.User,
                Name,
                (Hidden ? "8" : "0"),
                RealName
            );
        }

        public void SendPass(string Pass) {
            Send(Packet.Pass,
                Pass
            );
        }

        public void SendNick(string Nick) {
            Send(Packet.Nick,
                Nick
            );
        }

        public void SendOper(string Name, string Password) {
            Send(Packet.Oper,
                Name, Password
            );
        }

        public void SendMode(string Target, string Mode) {
            Send(Packet.ModeSingle,
                Target, Mode
            );
        }

        public void SendMode(string Convo, string Target, string Mode) {
            Send(Packet.ModeDouble,
                Convo, Mode, Target
            );
        }

        public void SendTopic(string Convo, string Topic) {
            Send(Packet.Topic,
                Convo, Topic
            );
        }

        public void SendKick(string Channel, string Target, string Message) {
            Send(Packet.Kick,
                Channel, Target, Message
            );
        }

        public void SendLUsers() {
            Send(Packet.LUsers);
        }

        public void SendMOTD() {
            Send(Packet.MOTD);
        }

        public void SendVersion() {
            Send(Packet.Version);
        }

        public void SendStats(string Flags) {
            Send(Packet.Stats,
                Flags
            );
        }

        public void SendLinks() {
            Send(Packet.Links);
        }

        public void SendTrace() {
            Send(Packet.Trace);
        }

        public void SendAdmin() {
            Send(Packet.Admin);
        }

        public void SendInfo() {
            Send(Packet.Info);
        }

        public void SendPong(string Message) {
            Send(Packet.Pong,
                Message
            );
        }

        public void JoinChannel(string Channel) {
            Send(Packet.Join,
                Channel
            );
            if (!this.Conversations.ContainsKey(Channel)) {
                this.Conversations.Add(
                    Channel,
                    new Conversation(Channel)
                );
            }
        }

        public void PartChannel(string Channel, string Message = "") {
            Send(Packet.Part,
                Channel, Message
            );
            if (this.Conversations.ContainsKey(Channel)) {
                this.Conversations.Remove(Channel);
            }
        }

        public void QuitServer(string Message = "") {
            Send(Packet.Quit,
                Message
            );
        }

        public void SendMessage(string Target, string Message) {
            Send(Packet.SendMsg,
                Target, Message
            );
        }

        public void SendMessage(Conversation Convo, string Message) {
            Send(Packet.SendMsg,
                Convo.Name, Message
            );
        }

        public void SendMessage(User Usr, string Message) {
            Send(Packet.SendMsg,
                Usr.Nick, Message
            );
        }

        public void SendNotice(string Target, string Message) {
            Send(Packet.Notice,
                Target, Message
            );
        }

        public void SendNotice(Conversation Convo, string Message) {
            Send(Packet.Notice,
                Convo.Name, Message
            );
        }

        public void SendNotice(User Usr, string Message) {
            Send(Packet.Notice,
                Usr.Nick, Message
            );
        }

        public void SendAction(string Target, string Message) {
            SendCTCP(Target,
                string.Format("ACTION {0}", Message)
            );
        }

        public void SendAction(Conversation Convo, string Message) {
            SendCTCP(Convo.Name,
                string.Format("ACTION {0}", Message)
            );
        }

        public void SendAction(User Usr, string Message) {
            SendCTCP(Usr.Nick,
                string.Format("ACTION {0}", Message)
            );
        }

        public void SendWho(string Nick) {
            Send(Packet.Who,
                Nick
            );
        }

        public void SendWhois(string Nick) {
            Send(Packet.Whois,
                Nick
            );
        }

        public void RegisterConnection() {
            if (!Connection.Connected)
                return;
            if (!string.IsNullOrEmpty(this.Password)) {
                SendPass(this.Password);
            }
            SendNick(this.Nick);
            SendUser(this.Username, this.Realname);
        }

        private string FixModes(string input) {
            return input.Replace("~", "q").Replace("+", "v").Replace("@", "o").Replace("&", "p");
        }
    }
}