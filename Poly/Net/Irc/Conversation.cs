﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Poly.Net.Irc {
	public class Conversation : Data.jsObject {
        public Conversation(string name = "") {
            Name = name;
        }

		public Conversation(Data.jsObject Obj) {
            foreach (var Key in Obj.Keys) {
                base[Key] = Obj[Key];
            }
		}

        public Data.jsObject Users {
            get {
                if (!this.ContainsKey("Users")) {
                    this["Users"] = new Data.jsObject();
                }
                return getObject("Users");
            }
        }

        public string Name {
            get {
                return getString("Name");
            }
            set {
                this["Name"] = value;
            }
        }

        public string Topic {
            get {
                return getString("Topic");
            }
            set {
                this["Topic"] = value;
            }
		}

		public string Mode {
			get {
				return getString("Mode");
			}
			set {
				if (value.StartsWith("+")) {
					this["Mode"] = Mode + value.Substring(1);
                } else if (value.StartsWith("-")) {
                    this["Mode"] = Mode.Replace(value.Substring(1), "");
                } else {
                    this["Mode"] = value;
				}
			}
		}

        public bool IsUserConversation() {
            return !Name.StartsWith("#");
        }
    }
}
