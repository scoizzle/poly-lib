﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

using Poly;
using Poly.Data;
using Poly.Net.Tcp;

namespace Poly.Net.Http {
    public partial class AppServer : jsObject {
        public HostFile Hosts = new HostFile();

        public Dictionary<int, Server> Listeners = new Dictionary<int, Server>();
        public jsObject<Http.OnAppRequestHandler> Routes = new jsObject<Http.OnAppRequestHandler>();

        public Chunker Chunker = new Chunker();

        public AppServer(string ConfigName) {
            Hosts.Load(ConfigName);
        }

        public AppServer(params jsObject[] HostConfigs) {
            foreach (var Host in HostConfigs) {
                Hosts.Add(new Host(Host));
            }
        }

        public Server GetListener(int Port) {
            if (!Listeners.ContainsKey(Port)) {
                return (Server)(Listeners[Port] = new Server(Port));
            }
            return Listeners[Port];
        }

        public void Start() {
            Hosts.ForEach((Key, Host) => {
                var Listen = GetListener(Host.Port);

                if (!Listen.Active) {
                    Listen.ClientConnect += OnClientConnect;
                    Listen.Start();
                }
            });
        }

        public void Stop() {
            foreach (var Listen in Listeners.Values) {
                Listen.Stop();
            }
        }
    }
}
