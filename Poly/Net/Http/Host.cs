﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;

namespace Poly.Net.Http {
    public class Host : jsObject {
        public Host() {
        }

        public Host(jsObject Base) {
            if (Base != null) {
                Base.CopyTo(this);
            }
        }

        public int Port {
            get {
                if (!this.ContainsKey("Port"))
                    return 80;
                return this.getInt("Port");
            }
            set {
                this["Port"] = value;
            }
        }

        public string Name {
            get {
                if (!this.ContainsKey("Name"))
                    return "localhost";
                return this.getString("Name");
            }
            set {
                this["Name"] = value;
            }
        }

        public string Path {
            get {
                if (!this.ContainsKey("Path"))
                    return "WWW/";
                return this.getString("Path");
            }
            set {
                this["Path"] = value;
            }
        }

        public string DefaultDocument {
            get {
                if (!this.ContainsKey("DefaultDocument"))
                    return "index.htm";
                return this.getString("DefaultDocument");
            }
            set {
                this["DefaultDocument"] = value;
            }
        }

        public string DefaultExtension {
            get {
                if (!this.ContainsKey("DefaultExtension"))
                    return "htm";
                return this.getString("DefaultExtension");
            }
            set {
                this["DefaultExtension"] = value;
            }
        }
    }

    public class HostList : jsObject<Host> {
        public override void OnParseObject(jsObject Object, params string[] Key) {
            Set(Key, new Host(Object));
        }
    }

    public class HostFile : jsFile<Host> {
        public override void OnParseObject(jsObject Object, params string[] Key) {
            Set(Key, new Host(Object));
        }
    }
}
