﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

using Poly;
using Poly.Data;
using Poly.Net.Tcp;

namespace Poly.Net.Http {
    public partial class FileServer {
        public Http.OnFileRequestHandler DefaultFileHandler = new Http.OnFileRequestHandler(
            (FileName, Request) => {
                Request.Result.Data = File.ReadAllBytes(FileName);
            }
        );

        public virtual void OnClientRequest(Request Request) {
            var WWW = Path.GetFullPath(
                Request.Host.Path + Path.DirectorySeparatorChar + Request.Packet.Target
            );
            var Ext = Path.GetExtension(WWW);

            if (string.IsNullOrEmpty(Ext) || WWW.EndsWith("/")) {
                var Temp = Path.GetFullPath(
                    WWW + Path.DirectorySeparatorChar + Request.Host.DefaultDocument
                );

                if (File.Exists(Temp)) {
                    WWW = Temp;
                    Ext = Path.GetExtension(Temp).Substring(1);
                }
            }

            if (File.Exists(WWW)) {
                var MIME = Mime.Types.ContainsKey(Ext.ToLower()) ? 
                    Mime.Types[Ext.ToLower()] : 
                    "application/octet-stream";

                var Handler = MimeHandlers.ContainsKey(MIME) ?
                    MimeHandlers[MIME] :
                    DefaultFileHandler;

                Request.Result.MIME = MIME;

                if (FileCache.Active && FileCache.IsCurrent(WWW)) {
                    CachedFile Object = FileCache[WWW];

                    Request.Result.MIME = Object.MIME;
                    Request.Result.Data = Object.Data;
                }
                else {
                    Handler(WWW, Request);
                }

                if (FileCache.Active && Request.Result.Data != null) {
                    CachedFile Obj = FileCache[WWW];

                    if (Obj != null) {
                        Obj.MIME = MIME;
                        Obj.Data = Request.Result.Data;
                    }
                    else {
                        FileCache[WWW] = new CachedFile(WWW) {
                            MIME = MIME,
                            Data = Request.Result.Data
                        };
                    }
                }
            }
            else {
                Request.Result = Result.NotFound;
            }
        }

        public virtual void SendReply(Client Client, Result Result) {
            if (Client.Connected && Result != null && Result.Data != null) {
                if (Chunker.Active && Result.Data.Length > Chunker.TriggerSize) {
                    Result.Headers.Add("Transfer-Encoding", "chunked");

                    Client.Send(
                        Result.BuildReply(true)
                    );

                    Chunker.Handle(Client, Result);
                }
                else if (Result.Data.Length > 0) {
                    Client.Send(
                        Result.BuildReply()
                    );

                    Client.GetStream().Write(Result.Data, 0, Result.Data.Length);
                }
            }
        }

        public virtual void OnClientConnect(Client Client) {
            Client.autoFlush = true;

            try {
                while (Client.Connected) {
                    Packet Packet = new Net.Http.Packet();

                    if (!Packet.Receive(Client)) {
                        SendReply(Client, Result.BadRequest);
                        Client.Close();
                        return;
                    }

                    Host Host = Hosts.Search<Host>(Packet.Host);

                    if (Host == null) {
                        SendReply(Client, Result.BadRequest);
                    }
                    else {
                        Request Request = new Request(Client, Packet, new Result()) {
                            Host = Host
                        };

                        OnClientRequest(Request);

                        SendReply(Client, Request.Result);
                    }
                }
            }
            catch {
                if (Client.Connected) {
                    Client.Close();
                }
            }
        }
    }
}