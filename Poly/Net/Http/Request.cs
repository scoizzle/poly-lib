﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;
using Poly.Net.Tcp;

namespace Poly.Net.Http {
    public class Request {
        public Client Client = null;
        public Packet Packet = null;
        public Result Result = null;

        public Host Host = null;
        public List<byte> Output = new List<byte>();

        public Request(Client Client, Packet Packet, Result Result) {
            this.Client = Client;
            this.Packet = Packet;
            this.Result = Result;
        }

        public Data.jsObject Get {
            get {
                return Packet.Get;
            }
        }

        public Data.jsObject Post {
            get {
                return Packet.Post;
            }
        }

        public Data.jsObject Cookies {
            get {
                return Packet.Cookies;
            }
        }

        public void Print(string txt) {
            if (!string.IsNullOrEmpty(txt)) {
                Output.AddRange(Client.Writer.Encoding.GetBytes(txt));
            }
        }

        public void Finish() {
            Result.Data = Output.ToArray();
            Output.Clear();
        }

        public void SetCookie(string name, string value, long expire = 0, string path = "", string domain = "", bool secure = false) {
            var Options = new jsObject();

            Options[name] = value;

            if (expire > 0) {
                Options["expire"] = DateTime.UtcNow.AddSeconds(expire).ToString("ddd, dd-MM-yyyy H:mm:ss UTC");
            }

            if (!string.IsNullOrEmpty(path))
                Options["path"] = path;

            if (!string.IsNullOrEmpty(domain))
                Options["domain"] = domain;

            if (secure)
                Options["secure"] = "true";

            Result.Cookies[name] = Options;
        }
    }
}
