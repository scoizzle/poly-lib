﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Poly;
using Poly.Net.Tcp;

namespace Poly.Net.Http {
    public class Chunker {
        public bool Active = true;
        public int MaxChunkSize = 1024 * 2;
        public int TriggerSize = 1024 * 1024;

        public void Handle(Client Client, Result Result) {
            byte[] Data = Result.Data;

            for (int Sent = 0; Sent < Data.Length; ) {
                int Size = Math.Min(MaxChunkSize, Data.Length - Sent);

                Client.Send(Size.ToString("X"));
                Client.GetStream().Write(Data, Sent, Size);
                Client.Send("");

                Sent += Size;
            }
            Client.Send("0");
        }
    }
}
