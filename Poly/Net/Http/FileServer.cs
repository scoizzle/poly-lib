﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets; 

using Poly;
using Poly.Data;
using Poly.Net.Tcp;

namespace Poly.Net.Http {
    public partial class FileServer : jsObject {
        public HostFile Hosts = new HostFile();

        public Dictionary<int, Server> Listeners = new Dictionary<int, Server>();
        public jsObject<Http.OnFileRequestHandler> MimeHandlers = new jsObject<Http.OnFileRequestHandler>();

        public FileCache FileCache = new FileCache();
        public Chunker Chunker = new Chunker();

        public FileServer(string ConfigName) {
            Hosts.Load(ConfigName);
        }

        public FileServer(params jsObject[] HostConfigurations) {
            foreach (var Host in HostConfigurations) {
                Hosts.Add(new Host(Host));
            }
        }

        public Server GetListener(int Port) {
            if (!Listeners.ContainsKey(Port)) {
                return (Server)(Listeners[Port] = new Server(Port));
            }
            return Listeners[Port];
        }

        public void Start() {
            Hosts.ForEach((Key, Host) => {
                var Listen = GetListener(Host.Port);

                if (!Listen.Active) {
                    Listen.ClientConnect += OnClientConnect;
                    Listen.Start();
                }
            });
        }

        public void Stop() {
            foreach (var Listen in Listeners.Values) {
                Listen.Stop();
            }
        }
    }
}
