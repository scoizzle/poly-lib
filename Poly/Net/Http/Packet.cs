﻿using System;
using System.Collections.Generic;
using System.Text;

using Poly;
using Poly.Data;

namespace Poly.Net.Http {
    public class Packet : Net.Tcp.Packet {
        public Data.jsObject Headers {
            get {
                if (!this.ContainsKey("Headers")) {
                    base["Headers"] = new Data.jsObject();
                }
                return getObject("Headers");
            }
        }

        public Data.jsObject Get {
            get {
                if (!this.ContainsKey("Get")) {
                    base["Get"] = new Data.jsObject();
                }
                return getObject("Get");
            }
        }

        public Data.jsObject Post {
            get {
                if (!this.ContainsKey("Post")) {
                    base["Post"] = new Data.jsObject();
                }
                return getObject("Post");
            }
        }

        public Data.jsObject Cookies {
            get {
                if (!this.ContainsKey("Cookies")) {
                    base["Cookies"] = new Data.jsObject();
                }
                return getObject("Cookies");
            }
        }

        public string Host {
            get {
                return Headers.getString("Host");
            }
            set {
                this.Headers["Host"] = value;
            }
        }

        public string RawTarget {
            get {
                return getString("RawTarget");
            }
            set {
                this["RawTarget"] = value;
            }
        }

        public string Connection {
            get {
                return getString("Connection");
            }
            set {
                this["Connection"] = value;
            }
        }

        public string Type {
            get {
                return getString("Type");
            }
            set {
                this["Type"] = value;
            }
        }

        public string Target {
            get {
                return getString("Target");
            }
            set {
                this["Target"] = value;
            }
        }

        public string Version {
            get {
                return getString("Version");
            }
            set {
                this["Version"] = value;
            }
        }

        public string Value {
            get {
                return getString("Value");
            }
            set {
                this["Value"] = Value;
            }
        }

        public override bool Receive(Net.Tcp.Client Client) {
            if (!Client.Connected)
                return false;

            var Content_Length = 0;
            var Protocol = Client.Reader.ReadLine().Match("{Type} {Target} {Version}");

            if (Protocol == null || Protocol.IsEmpty)
                return false;
            else
                Protocol.CopyTo(this);

            this["RawTarget"] = this["Target"];

            do {
                string Line = Client.Reader.ReadLine();

                if (string.IsNullOrEmpty(Line))
                    break;

                if (Line.Contains(": ")) {
                    if (Line.StartsWith("Content-Length")) {
                        Content_Length = int.Parse(Line.Substring(": "));
                    }
                    this.Headers[Line.Substring("", ":")] = Line.Substring(": ");
                }
                else {
                    return false;
                }
            } while (true);

            if (Content_Length == 0)
                return true;

            StringBuilder Output = new StringBuilder();
            while (Content_Length > 0) {
                char[] Buffer = new char[1024];

                Content_Length -= Client.Reader.ReadBlock(Buffer, 0, Math.Min(1024, Content_Length));

                Output.Append(Buffer);
            }
            Value = Output.ToString();
            Output = null;

            if (Target.Contains("?")) {
                var Args = Target.Substring("?", "").Split('&');
                Target = Target.Substring("", "?");

                foreach (var RawPair in Args) {
                    var Key = RawPair.Substring("", "=");
                    var Val = RawPair.Substring("=");

                    Get[Key] = Val;
                }
            }

            if (Type == "POST") {
                string[] Split = Value.Split('&');

                foreach (var Pair in Split) {
                    if (Pair.Contains("=")) {
                        Post.Add(
                            Pair.Substring("", "="),
                            Pair.Substring("=")
                        );
                    }
                }
            }

            if (Headers.ContainsKey("Cookie")) {
                var Split = getString("Cookie").Split(';');

                foreach (var Pair in Split) {
                    if (Pair.Contains("=")) {
                        Cookies.Add(
                            Pair.Substring("", "="),
                            Pair.Substring("=")
                        );
                    }
                }

                Headers.Remove("Cookie");
            }

            return true;
        }
    }
}