﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Poly;
using Poly.Data;

namespace Poly.Net.Http {
    public class CachedFile {
        public string   Name = "";
        public string   MIME = "text/html";
        public byte[]   Data = new byte[0];
        public DateTime LastWriteTime = default(DateTime);

        public CachedFile(string Name) {
            if (File.Exists(Name)) {
                this.Name = Name;
                this.LastWriteTime = File.GetLastWriteTime(Name);
            }
        }
    }

    public class FileCache : jsObject<CachedFile> {
        public bool Active = true;

        public CachedFile this[params string[] Key] {
            get {
                return base[Key[0].Base64Encode()];
            }
            set {
                base[Key[0].Base64Encode()] = value;
            }
        }

        public bool IsCurrent(string ObjectName) {
            CachedFile Obj = this[ObjectName];

            if (Obj == null || !File.Exists(ObjectName))
                return false;

            if (Obj.LastWriteTime == File.GetLastWriteTime(ObjectName)) {
                return true;
            }
            return false;
        }
    }
}
