﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly.Data;
using Poly.Net.Tcp;

namespace Poly.Net.Http {
    public class Result : Data.jsObject {
        public string Status {
            get {
                if (!this.ContainsKey("Status"))
                    return "200 Ok";
                return getString("Status");
            }
            set {
                this["Status"] = value;
            }
        }

        public string MIME {
            get {
                if (!this.ContainsKey("MIME"))
                    return "text/html";
                return getString("MIME");
            }
            set {
                this["MIME"] = value;
            }
        }

        public Data.jsObject Cookies {
            get {
                if (!this.ContainsKey("Cookies"))
                    this["Cookies"] = new Data.jsObject();
                return getObject("Cookies");
            }
            set {
                this["Cookies"] = value;
            }
        }

        public Data.jsObject Headers {
            get {
                if (!this.ContainsKey("Headers"))
                    this["Headers"] = new Data.jsObject();
                return getObject("Headers");
            }
            set {
                this["Headers"] = value;
            }
        }

        public byte[] Data {
            get {
                if (!ContainsKey("Data")) {
                    this["Data"] = new byte[0];
                }
                return getObject<byte[]>("Data");
            }
            set {
                this["Data"] = value;
            }
        }

        public string BuildReply(bool Chunked = false) {
            List<string> Headers = new List<string>() {
                "HTTP/1.1 " + this.Status,
                "Date: " + DateTime.Now.HttpTimeString()
            };

            if (!Chunked) {
                if (this.Data != null && this.Data.Length > 0) {
                    Headers.Add("Content-Type: " + this.MIME);
                    Headers.Add("Content-Length: " + this.Data.Length.ToString());
                }
                else {
                    Headers.Add("Content-Length: 0");
                }
            }

            foreach (var Head in this.Headers) {
                Headers.Add(Head.Key + ": " + Head.Value);
            }

            foreach (jsObject Cookie in this.Cookies.Values) {
                StringBuilder CookieString = new StringBuilder();

                foreach (var Option in Cookie) {
                    CookieString.Append(Option.Key + "=" + Option.Value);
                }

                Headers.Add(CookieString.ToString());
            }

            return string.Join(Environment.NewLine, Headers) + Environment.NewLine;
        }

        public static readonly Result Ok = new Result() {
            Status = "200 Ok"
        };

        public static readonly Result Created = new Result() {
            Status = "201 Created"
        };

        public static readonly Result Accepted = new Result() {
            Status = "202 Accepted"
        };

        public static readonly Result PartialInfo = new Result() {
            Status = "203 Partial Information"
        };

        public static readonly Result NoResponse = new Result() {
            Status = "204 No Response"
        };

        public static readonly Result ResetContent = new Result() {
            Status = "205 Reset Content"
        };

        public static readonly Result PartialContent = new Result() {
            Status = "206 Partial Content"
        };

        public static readonly Result Moved = new Result() {
            Status = "301 Moved"
        };

        public static readonly Result Found = new Result() {
            Status = "302 Found"
        };

        public static readonly Result Method = new Result() {
            Status = "303 Method"
        };

        public static readonly Result NotModified = new Result() {
            Status = "304 Not Modified"
        };

        public static readonly Result BadRequest = new Result() {
            Status = "400 Bad Request"
        };

        public static readonly Result Unauthorized = new Result() {
            Status = "401 Unauthorized"
        };

        public static readonly Result PaymentRequired = new Result() {
            Status = "402 PaymentRequired"
        };

        public static readonly Result Forbidden = new Result() {
            Status = "403 Forbidden"
        };

        public static readonly Result NotFound = new Result() {
            Status = "404 Not Found"
        };

        public static readonly Result InternalError = new Result() {
            Status = "500 Internal Server Error" 
        };

        public static readonly Result NotImplemented = new Result() {
            Status = "501 Not Implemented"
        };
    }
}
