﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

using Poly;
using Poly.Data;
using Poly.Net.Tcp;

namespace Poly.Net.Http {
    public partial class AppServer : jsObject {
        public virtual void OnClientRequest(Request Request) {
            Routes.ForEach((Path, Handler) => {
                jsObject Data = Request.Packet.Target.Match(Path);

                if (Data != null) {
                    (Handler as Http.OnAppRequestHandler)(Data, Request);
                    return;
                }
            });
        }

        public virtual void SendReply(Client Client, Result Result) {
            if (Client.Connected && Result != null && Result.Data != null) {
                if (Chunker.Active && Result.Data.Length > Chunker.TriggerSize) {
                    Result.Headers.Add("Transfer-Encoding", "chunked");

                    Client.Send(
                        Result.BuildReply(true)
                    );

                    Chunker.Handle(Client, Result);
                }
                else if (Result.Data.Length > 0) {
                    Client.Send(
                        Result.BuildReply()
                    );

                    Client.GetStream().Write(Result.Data, 0, Result.Data.Length);
                }
            }
        }

        public virtual void OnClientConnect(Client Client) {
            Client.autoFlush = true;

            while (Client.Connected) {
				Packet Packet = new Net.Http.Packet();

                if (!Packet.Receive(Client)) {
                    SendReply(Client, Result.BadRequest);
					Client.Close();
					return;
                }

                Host Host = Hosts.Search<Host>(Packet.Host);

                if (Host == null) {
                    SendReply(Client, Result.BadRequest);
                }
                else {
                    Request Request = new Request(Client, Packet, new Result()) {
						Host = Host
                    };

                    OnClientRequest(Request);

                    SendReply(Client, Request.Result);
                }
            }
        }
    }
}