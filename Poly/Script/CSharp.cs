﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Poly.Script {
    public class CSharp : DynamicCodeCompiler {
        public bool Prepared = false;

        public string FileName = string.Empty;
        public string Path = Environment.CurrentDirectory;

        public Dictionary<string, DateTime> IncludedFiles = new Dictionary<string, DateTime>(8);

        public CSharp(string ClassName = "CSharp.Script", string EntryPoint = "Main", bool Debug = false, params string[] Assemblies) :
            base(ClassName, Debug, Assemblies) {
                this.EntryPoint = EntryPoint;
        }

        public bool IsCurrent {
            get {
                if (!Prepared)
                    return false;

                foreach (string FileName in IncludedFiles.Keys)
                    try {
                        var Time = IncludedFiles[FileName];

                        if (Time != File.GetLastWriteTime(FileName)) {
                            return false;
                        }
                    }
                    catch (Exception Error) {
                        Application.Log.Error(Error.ToString());
                    }

                return true;
            }
        }

        public void Load(string fileName = "") {
            if (!string.IsNullOrEmpty(fileName)) {
                this.FileName = fileName;
                this.IncludedFiles[fileName] = File.GetLastWriteTime(fileName);
            }
            try {
                Prepare();
            }
            catch {
                Application.Log.Error("Failed to load CSharp Script: " + FileName);
            }
        }

        public void Prepare() {
            string Code = System.IO.File.ReadAllText(FileName);

            if (string.IsNullOrEmpty(Code))
                return;

            foreach (string Line in Code.Split(Environment.NewLine)) {
                if (Line.StartsWith("// ref ")) {
                    CompilerParams.ReferencedAssemblies.Add(
                        Line.Substring("// ref ", "").Trim()
                    );
                    continue;
                }

                if (Line.StartsWith("// embed ")) {
                    string File = Line.Substring("// embed ", "").Trim();

                    if (File.StartsWith("~")) {
                        File = Path + File.Substring(1);
                    }

                    System.IO.FileInfo Info = new System.IO.FileInfo(File);

                    if (Info.Exists) {
                        this.CompilerParams.EmbeddedResources.Add(
                            Info.FullName
                        );
                        IncludedFiles[Info.FullName] = System.IO.File.GetLastWriteTime(File);
                    }
                    continue;
                }

                if (Line.StartsWith("// include ")) {
                    string File = Line.Substring(
                        "// include ", ""
                    ).Trim();

                    if (File.StartsWith("~")) {
                        File = System.IO.Path.GetFullPath(Path + File.Substring(1));
                    }

                    System.IO.FileInfo Info = new System.IO.FileInfo(File);

                    if (Info.Exists) {
                        if (IncludedFiles.ContainsKey(Info.FullName)) {
                            continue;
                        }

                        CSharp Include = new CSharp();

                        Include.Path = Path;
                        Include.IncludedFiles = IncludedFiles;

                        Include.Load(Info.FullName);

                        IncludedFiles[Info.FullName] = System.IO.File.GetLastWriteTime(File);

                        foreach (var Ref in Include.CompilerParams.ReferencedAssemblies) {
                            if (!this.CompilerParams.ReferencedAssemblies.Contains(Ref))
                                CompilerParams.ReferencedAssemblies.Add(Ref);
                        }

                        foreach (var Emb in Include.CompilerParams.EmbeddedResources) {
                            this.CompilerParams.EmbeddedResources.Add(Emb);
                            IncludedFiles[Emb] = System.IO.File.GetLastWriteTime(Emb);
                        }
                    }
                    continue;
                }

                if (Line.StartsWith("// class ")) {
                    this.MainClass = Line.Substring(
                        "// class ", ""
                    ).Trim();
                    continue;
                }

                if (Line.StartsWith("// main ")) {
                    this.EntryPoint = Line.Substring(
                        "// main ", ""
                    ).Trim();
                    continue;
                }
            }
            Prepared = true;
        }

        public bool Compile() {
            if (!Prepared)
                Prepare();

            var Files = IncludedFiles.Keys.ToArray();

            CompilerResults = CompilerInterface.CompileAssemblyFromFile(CompilerParams, Files);

            return !CompilerResults.Errors.HasErrors;
        }
    }
}
