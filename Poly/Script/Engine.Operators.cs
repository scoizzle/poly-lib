﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;

namespace Poly.Script {
    public partial class Engine : jsObject {
        public class Assign : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public Assign(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                if (Left is Variable) {
                    var Name = (Left as Variable).FixName(Context);
                    var leftVal = this.Left.Evaluate(Context);

                    if (Right is Function) {
                        Context[Name] = Right;
                        return new Void();
                    }
                    else {
                        var rightVal = this.Right.Evaluate(Context);

                        if (rightVal is Number) {
                            Context[Name] = (rightVal as Number).Value;
                        }
                        else if (rightVal is Bool) {
                            Context[Name] = (rightVal as Bool).Value;
                        }
                        else if (rightVal is String) {
                            Context[Name] = (rightVal as String).Value;
                        }
                        else if (rightVal is Object) {
                            Context[Name] = (rightVal as Object).Base;
                        }
                        else {
                            Context[Name] = rightVal;
                        }

                        return rightVal;
                    }
                }
                else {
                    return new Value();
                }
            }

            public override string ToString() {
                return Left.ToArray() + " = " + Right.ToString();
            }
        }

        public new class Add : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public Add(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Number(
                        (leftVal as Number).Value + (rightVal as Number).Value
                    );
                }
                else {
                    return new String(
                        leftVal.ToString() + rightVal.ToString()
                    );
                }
            }

            public override string ToString() {
                return Left.ToArray() + " + " + Right.ToString();
            }
        }

        public class Subtract : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public Subtract(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Number(
                        (leftVal as Number).Value - (rightVal as Number).Value
                    );
                }
                else {
                    return new Value();
                }
            }

            public override string ToString() {
                return Left.ToArray() + " - " + Right.ToString();
            }
        }


        public class Multiply : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public Multiply(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Number(
                        (leftVal as Number).Value * (rightVal as Number).Value
                    );
                }
                else {
                    return new Value();
                }
            }

            public override string ToString() {
                return Left.ToArray() + " * " + Right.ToString();
            }
        }

        public class Devide : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public Devide(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Number(
                        (leftVal as Number).Value / (rightVal as Number).Value
                    );
                }
                else {
                    return new Value();
                }
            }

            public override string ToString() {
                return Left.ToArray() + " / " + Right.ToString();
            }
        }

        public class Equal : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public Equal(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Bool(
                        (leftVal as Number).Value == (rightVal as Number).Value
                    );
                }
                else if (leftVal is Bool && rightVal is Bool) {
                    return new Bool(
                        (leftVal as Bool).Value == (rightVal as Bool).Value
                    );
                }
                else if (leftVal is Object && rightVal is Object) {
                    return new Bool(
                        (leftVal as Object).Equals(rightVal as Object)
                    );
                }
                else {
                    return new Bool(
                        leftVal.ToString() == rightVal.ToString()
                    );
                }
            }

            public override string ToString() {
                return Left.ToArray() + " == " + Right.ToString();
            }
        }

        public class NotEqual : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public NotEqual(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Bool(
                        (leftVal as Number).Value != (rightVal as Number).Value
                    );
                }
                else if (leftVal is Bool && rightVal is Bool) {
                    return new Bool(
                        (leftVal as Bool).Value != (rightVal as Bool).Value
                    );
                }
                else if (leftVal is Object && rightVal is Object) {
                    return new Bool(
                        !(leftVal as Object).Equals(rightVal as Object)
                    );
                }
                else {
                    return new Bool(
                        leftVal.ToString() != rightVal.ToString()
                    );
                }
            }

            public override string ToString() {
                return Left.ToArray() + " != " + Right.ToString();
            }
        }

        public class LessThan : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public LessThan(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Bool(
                        (leftVal as Number).Value < (rightVal as Number).Value
                    );
                }
                else if (leftVal is String && rightVal is String) {
                    if ((leftVal as String).Value.Length == 1 && (rightVal as String).Value.Length == 1) {
                        return new Bool(
                            (leftVal as String).Value[0] < (rightVal as String).Value[0]
                        );
                    }
                }
                return new Value();
            }

            public override string ToString() {
                return Left.ToArray() + " < " + Right.ToString();
            }
        }

        public class GreaterThan : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public GreaterThan(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Bool(
                        (leftVal as Number).Value > (rightVal as Number).Value
                    );
                }
                else if (leftVal is String && rightVal is String) {
                    if ((leftVal as String).Value.Length == 1 && (rightVal as String).Value.Length == 1) {
                        return new Bool(
                            (leftVal as String).Value[0] > (rightVal as String).Value[0]
                        );
                    }
                }
                return new Value();
            }

            public override string ToString() {
                return Left.ToArray() + " > " + Right.ToString();
            }
        }

        public class LessThanEqualTo : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public LessThanEqualTo(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Bool(
                        (leftVal as Number).Value <= (rightVal as Number).Value
                    );
                }
                else if (leftVal is String && rightVal is String) {
                    if ((leftVal as String).Value.Length == 1 && (rightVal as String).Value.Length == 1) {
                        return new Bool(
                            (leftVal as String).Value[0] <= (rightVal as String).Value[0]
                        );
                    }
                }
                return new Value();
            }

            public override string ToString() {
                return Left.ToArray() + " <= " + Right.ToString();
            }
        }

        public class GreaterThanEqualTo : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public GreaterThanEqualTo(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Number && rightVal is Number) {
                    return new Bool(
                        (leftVal as Number).Value >= (rightVal as Number).Value
                    );
                }
                else if (leftVal is String && rightVal is String) {
                    if ((leftVal as String).Value.Length == 1 && (rightVal as String).Value.Length == 1) {
                        return new Bool(
                            (leftVal as String).Value[0] >= (rightVal as String).Value[0]
                        );
                    }
                }
                return new Value();
            }

            public override string ToString() {
                return Left.ToArray() + " >= " + Right.ToString();
            }
        }

        public class And : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public And(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Bool && rightVal is Bool) {
                    return new Bool(
                        (leftVal as Bool).Value && (rightVal as Bool).Value
                    );
                }
                else {
                    return new Value();
                }
            }

            public override string ToString() {
                return Left.ToArray() + " && " + Right.ToString();
            }
        }

        public class Or : Operator {
            private Node Left {
                get {
                    if (!ContainsKey("Left")) {
                        return new Node();
                    }
                    return getObject<Node>("Left");
                }
                set {
                    this["Left"] = value;
                }
            }

            private Node Right {
                get {
                    if (!ContainsKey("Right")) {
                        return new Node();
                    }
                    return getObject<Node>("Right");
                }
                set {
                    this["Right"] = value;
                }
            }

            public Or(Node Left, Node Right) {
                this.Left = Left;
                this.Right = Right;
            }

            public override Value Evaluate(jsObject Context) {
                var leftVal = this.Left.Evaluate(Context);
                var rightVal = this.Right.Evaluate(Context);

                if (leftVal is Bool && rightVal is Bool) {
                    return new Bool(
                        (leftVal as Bool).Value || (rightVal as Bool).Value
                    );
                }
                else {
                    return new Value();
                }
            }

            public override string ToString() {
                return Left.ToArray() + " || " + Right.ToString();
            }
        }

        public class Break : Operator {
            public override Value Evaluate(jsObject Context) {
                return this;
            }

            public override string ToString() {
                return "break";
            }
        }

        public class Continue : Operator {
            public override Value Evaluate(jsObject Context) {
                return this;
            }

            public override string ToString() {
                return "continue";
            }
        }

        public class Delete : Operator {
            public string Name {
                get {
                    if (!ContainsKey("Name"))
                        return "";
                    return getString("Name");
                }
                set {
                    this.Base["Name"] = value;
                }
            }

            public Delete(string Name) {
                this.Name = Name;
            }

            public override Value Evaluate(jsObject Context) {
                object obj = Context[Name];

                if (obj != null)
                    Context[Name] = null;

                return new Void();
            }

            public override string ToString() {
                return "delete " + Name;
            }
        }
    }
}
