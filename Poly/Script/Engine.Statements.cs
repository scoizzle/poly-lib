﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Poly;
using Poly.Data;

namespace Poly.Script {
    public partial class Engine : jsObject {
        public class If : Statement {
            public Node Boolean = new Node();
            public Node Else = new Node();

            public override Value Evaluate(jsObject Context) {
                if (Engine._evalBooleanNode(Boolean, Context)) {
                    return base.Evaluate(Context);
                }
                else if (!Else.IsEmpty) {
                    return Else.Evaluate(Context);
                }
                return new Value();
            }

            public override string ToString() {
                return "if (" + Boolean.ToString() + ")";
            }
        }

        public class For : Statement {
            public Node Init = new Node(), Boolean = new Node(), Modifier = new Node();

            public override Value Evaluate(jsObject Context) {
                Init.Evaluate(Context);

                for (var BoolResult = (Value)null;
                        (BoolResult = Boolean.Evaluate(Context)) is Bool && (BoolResult as Bool).Value;
                        Modifier.Evaluate(Context)
                    ) {
                    foreach (Node Node in Values) {
                        var Result = Node.Evaluate(Context);

                        if (Node is Return) {
                            return Result;
                        }
                        else if (Node is Break || Result is Break) {
                            return new Void();
                        }
                        else if (Node is Continue || Result is Continue) {
                            break;
                        }
                    }
                }

                return new Void();
            }

            public override string ToString() {
                return "for (" + Init.ToString() + "; " + Boolean.ToString() + "; " + Modifier.ToString() + ")";
            }
        }

        public class Foreach : Statement {
            public string VarName = "";
            public Node Object = new Node();

            public override Value Evaluate(jsObject Context) {
                var Values = Object.Evaluate(Context);

                if (Values is Object) {
                    foreach (var Sub in (Values as Object)) {
                        foreach (Node Node in this.Values) {
                            Context[VarName] = Sub;
                            var Result = Node.Evaluate(Context);

                            if (Node is Return) {
                                return Result;
                            }
                            else if (Node is Break) {
                                return new Void();
                            }
                            else if (Node is Continue) {
                                break;
                            }
                        }
                    }
                }
                else if (Values is String) {
                    foreach (var Sub in (Values as String).Value) {
                        foreach (Node Node in this.Values) {
                            Context[VarName] = Sub;
                            var Result = Node.Evaluate(Context);

                            if (Node is Return) {
                                return Result;
                            }
                            else if (Node is Break || Result is Break) {
                                return new Void();
                            }
                            else if (Node is Continue || Result is Continue) {
                                break;
                            }
                        }
                    }
                }
                return new Void();
            }

            public override string ToString() {
                return "foreach (" + VarName + " in " + Object.ToString() + ")";
            }
        }

        public class Do : Statement {
            public Node Boolean = new Node();

            public override Value Evaluate(jsObject Context) {
                do {
                    foreach (Node Node in this.Values) {
                        var Result = Node.Evaluate(Context);

                        if (Node is Return) {
                            return Result;
                        }
                        else if (Node is Break || Result is Break) {
                            return new Void();
                        }
                        else if (Node is Continue || Result is Continue) {
                            break;
                        }
                    }
                } while (_evalBooleanNode(Boolean, Context));

                return new Void();
            }

            public override string ToString() {
                return "do { } while (" + Boolean.ToString() + ");";
            }
        }

        public class While : Statement {
            public Node Boolean = new Node();

            public override Value Evaluate(jsObject Context) {
                while (_evalBooleanNode(Boolean, Context)) {
                    foreach (Node Node in this.Values) {
                        var Result = Node.Evaluate(Context);

                        if (Node is Return) {
                            return Result;
                        }
                        else if (Node is Break || Result is Break) {
                            return new Void();
                        }
                        else if (Node is Continue || Result is Continue) {
                            break;
                        }
                    }
                }

                return new Void();
            }

            public override string ToString() {
                return "while (" + Boolean.ToString() + ")";
            }
        }

        public class Async : Statement {
            public Expression Expression {
                get {
                    return getObject<Expression>("Expression");
                }
                set {
                    this["Expression"] = value;
                }
            }

            public Async(Expression Expression) {
                this.Expression = Expression;
            }

            public override Value Evaluate(jsObject Context) {
                ThreadPool.QueueUserWorkItem(
                    new WaitCallback(
                        (State) => {
                            Expression.Evaluate(State as jsObject);
                        }
                    ),
                    Context
                );
                return new Void();
            }

            public override string ToString() {
                return "async " + Expression.ToString();
            }
        }

        public class Function : Statement {
            public string Name = "";
            public List<string> Arguments = new List<string>();

            public override Value Evaluate(jsObject Context) {
                foreach (Node Node in this.Values) {
                    if (Node == null || Node is Function)
                        continue;

                    var Result = Node.Evaluate(Context);

                    if (Node is Return) {
                        return Result;
                    }
                }
                return new Void();
            }

            public override string ToString() {
                return string.Format("{0}({1})", Name, string.Join(", ", Arguments));
            }
        }

        public class SystemFunction : Function {
            public Func<jsObject, Value> Delegate;

            public SystemFunction() {
            }

            public SystemFunction(string Name, Func<jsObject, Value> Handler, params string[] ArgNames) {
                this.Name = Name;
                this.Delegate = Handler;
                this.Arguments.AddRange(ArgNames);
            }

            public override Value Evaluate(jsObject Arguments) {
                if (Delegate == null)
                    throw new Exception("Function null.");

                return Delegate(Arguments);
            }

            public override string ToString() {
                return base.ToString() + " [System]";
            }
        }

        public class Call : Statement {
            public Function Function = new Function();
            public Dictionary<string, Node> Arguments = new Dictionary<string, Node>();

            public override Value Evaluate(jsObject Context) {
                using (var Args = new jsObject()) {
                    foreach (var Pair in Arguments) {
                        var Value = Pair.Value.Evaluate(Context);
                        var Key = Pair.Key;

                        if (Context[Key] != null) {
                            Args[Key] = Context[Key];
                        }

                        if (Value.ContainsKey("Value")) {
                            Context[Key] = Value.Get("Value");
                        }
                        else {
                            Context[Key] = Value;
                        }
                    }

                    var Return = Function.Evaluate(Context);

                    Args.CopyTo(Context);

                    return Return;
                }
            }

            public override string ToString() {
                return string.Format("{0}({1})", Function.Name, string.Join(", ", Arguments.Keys));
            }
        }

        public class ContextCall : Statement {
            public bool Async = false;

            public string Name {
                get {
                    if (!this.ContainsKey("Name"))
                        return "";
                    return getString("Name");
                }
                set {
                    this.Base["Name"] = value;
                }
            }

            public Engine Engine {
                get {
                    if (!this.ContainsKey("Engine"))
                        return null;
                    return getObject<Engine>("Engine");
                }
                set {
                    this.Base["Engine"] = value;
                }
            }

            public List<Node> Arguments {
                get {
                    if (!this.ContainsKey("Arguments")) {
                        this.Base["Arguments"] = new List<Node>();
                    }
                    return getObject<List<Node>>("Arguments");
                }
                set {
                    this.Base["Arguments"] = value;
                }
            }

            public override Value Evaluate(jsObject Context) {
                if (string.IsNullOrEmpty(Name) || Engine == null)
                    return new Value();
                var Function = Engine.GetFunction(Name, Context);

                if (Function == null) {
                    Application.Log.Error("Function " + Name + " not defined!");
                    return null;
                }

                if (Function.Arguments.Count != Arguments.Count) {
                    Application.Log.Error("Calling " + Name + " with incorrect arguments.");
                    return null;
                }

                using (var Args = new jsObject()) {
                    for (int x = 0; x < Arguments.Count; x++) {
                        var Value = Arguments[x].Evaluate(Context);
                        var Key = Function.Arguments[x++];

                        if (Context[Key] != null) {
                            Args[Key] = Context[Key];
                        }

                        if (Value.ContainsKey("Value")) {
                            Context[Key] = Value.Get("Value");
                        }
                        else {
                            Context[Key] = Value;
                        }
                    }

                    if (Name.Contains('.')) {
                        Context["this"] = Context[Name.Substring(0, Name.LastIndexOf('.'))];
                        Args.Set("this", null);
                    }

                    var Return = Function.Evaluate(Context);

                    Args.CopyTo(Context);

                    return Return;
                }
            }

            public override string ToString() {
                return string.Format("{0}({1})", Name, string.Join(", ", Arguments));
            }
        }

        public class Return : Statement {
            private Node Value {
                get {
                    if (!ContainsKey("Value")) {
                        return new Node();
                    }
                    return getObject<Node>("Value");
                }
                set {
                    this["Value"] = value;
                }
            }

            public Return(Node Value) {
                this.Value = Value;
            }

            public override Value Evaluate(jsObject Context) {
                return Value.Evaluate(Context);
            }

            public override string ToString() {
                return "return " + Value.ToString();
            }
        }
    }
}
