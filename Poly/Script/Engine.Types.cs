﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;

namespace Poly.Script {
    public partial class Engine : jsObject {
        public class Node : jsObject<Node> {
            public virtual Value Evaluate(jsObject Context) {
                return new Value();
            }

            public override string ToString() {
                return "";
            }

            public override string ToString(bool HumanFormat) {
                return this.ToString();
            }
        }

        public class Value : Node {
            public override Value Evaluate(jsObject Context) {
                return this;
            }
        }

        public class Expression : Value {
            public Expression(params Node[] Nodes) {
                foreach (var Node in Nodes) {
                    this.Add(Node);
                }
            }

            public override Value Evaluate(jsObject Context) {
                foreach (Node Node in Values) {
                    if (Node == null || Node is Function)
                        continue;
                    
                    if (Node is Number || Node is String || Node is Bool || Node is Object) {
                        return (Value)Node;
                    }

                    var Result = Node.Evaluate(Context);

                    if (Node is Return)
                        return Result;

                    if (Node is Operator) {
                        if (this.Count == 1)
                            return Result;

                        if (Node is Break || Node is Continue) {
                            return Result;
                        }
                        continue;
                    }

                    if (Result is Break || Result is Continue || Result is Number || Result is String || Result is Bool || Result is Object) {
                        return Result;
                    }
                }
                return new Void();
            }
        }

        public class Nop : Expression {
        }

        public class Statement : Expression {
        }

        public class Operator : Statement {
        }

        public class Variable : Value {
            public string Name {
                get {
                    if (!this.ContainsKey("Name"))
                        return "";
                    return getString("Name");
                }
                set {
                    this.Base["Name"] = value;
                }
            }

            public string FixName(jsObject Context) {
                var Name = this.Name;

                if (this.Count > 1) {
                    List<string> Results = new List<string>();

                    foreach (var Item in this.Values) {
                        if (Item is Node) {
                            Results.Add((Item as Node).Evaluate(Context).ToString());
                        }
                    }

                    Name = string.Format(Name, Results.ToArray());
                }

                return Name;
            }

            public Variable(string Name) {
                this.Name = Name;
            }

            public override Value Evaluate(jsObject Context) {
                var Name = FixName(Context);
                var Res = Context[Name];

                if (Res != null) {
                    if (Res is Value) {
                        return (Value)Res;
                    }
                    else if (Res is string) {
                        return new String((string)Res);
                    }
                    else if (Res is int || Res is double) {
                        return new Number((double)Res);
                    }
                    else if (Res is bool) {
                        return new Bool((bool)Res);
                    }
                    else if (Res is jsObject<Node>) {
                        return new Object(Res as jsObject<Node>);
                    }
                    else {
                        return new String(Res.ToString());
                    }
                }
                else {
                    return new Value();
                }
            }

            public override string ToString() {
                return Name;
            }
        }

        public class Void : Value {
            public override string ToString() {
                return string.Empty;
            }
        }

        public class Bool : Value {
            public bool Value {
                get {
                    if (!ContainsKey("Value"))
                        return default(bool);
                    return getBool("Value");
                }
                set {
                    this.Base["Value"] = value;
                }
            }

            public Bool(bool val) {
                Value = val;
            }

            public override string ToString() {
                return Value.ToString();
            }
        }

        public class Number : Value {
            public double Value {
                get {
                    if (!ContainsKey("Value"))
                        return default(double);
                    return getDouble("Value");
                }
                set {
                    this.Base["Value"] = value;
                }
            }

            public Number(double num) {
                Value = num;
            }

            public override string ToString() {
                return Value.ToString();
            }
        }

        public class String : Value {
            public string Value {
                get {
                    if (!ContainsKey("Value"))
                        return "";
                    return getString("Value");
                }
                set {
                    Set("Value", value);
                }
            }

            public String(string str) {
                Value = str;
            }

            public override string ToString() {
                return Value.ToString();
            }
        }

        public class Object : Value {
            public Object(jsObject<Node> Obj) {
                Obj.CopyTo(this);
            }

            public override string ToString() {
                return this.Base.ToString();
            }
        }
    }
}
