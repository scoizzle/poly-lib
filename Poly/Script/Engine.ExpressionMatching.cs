﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;

namespace Poly.Script {
    public partial class Engine : jsObject {
        public delegate Expression ExpressionParser(Engine This, string Text, ref int Index);

        public static List<ExpressionParser> ExpressionParsers = new List<ExpressionParser>() {
            __If,
            __For,
            __Foreach,
            __While,
            __Do,
            __Unary,
            __Return,
            __Delete,
            __CodeBlock,
            __EvalBlock,
            __Using,
            __String,
            __Number,
            __Boolean,
            __Variable,
            __Async,
            __FunctionDefinition,
            __FunctionCall
        };

        private static bool _isValidVariableName(string Text) {
            if (string.IsNullOrEmpty(Text))
                return false;

            Text = Text.Trim();

            for (int Index = 0; Index < Text.Length; Index++) {
                char c = Text[Index];

                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c > '0' && c < '9') || (c == '_') || (c == '.') || (c == '[') || (c == ']'))
                    continue;
                else
                    return false;
            }

            return true;
        }

        private static void _parseInto(Engine This, Expression Expression, string Text) {
            var Node = This._Parse(Text.Trim());

            if (Node is Expression) {
                Node.CopyTo(Expression);
            }
            else {
                Expression.Add(Node);
            }
        }

        public static void ConsumeWhitespace(string Text, ref int Index) {
            if (!string.IsNullOrEmpty(Text))
                while (Index < Text.Length && char.IsWhiteSpace(Text[Index]))
                    Index++;
        }

        public static void ConsumeContent(string Text, ref int Index) {
            while (Index < Text.Length) {
                char c = Text[Index];

                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || (c == '_') || (c == '.')) {
                    Index++;
                }
                else if (c == '(') {
                    var Temp = Text.FindMatchingBrackets("(", ")", Index);

                    Index += Temp.Length + 2;
                }
                else if (c == '[') {
                    var Temp = Text.FindMatchingBrackets("[", "]", Index);

                    Index += Temp.Length + 2;
                }
                else if (c == '"') {
                    var Temp = Text.FindMatchingBrackets("\"", "\"", Index);

                    Index += Temp.Length + 2;
                }
                else if (c == '\'') {
                    var Temp = Text.FindMatchingBrackets("'", "'", Index);

                    Index += Temp.Length + 2;
                }
                else {
                    break;
                }
            }
        }

        public static Variable __ParseVar(Engine This, string Name) {
            if (Name.IndexOf('[') > 0 && Name.IndexOf('[') < Name.IndexOf(']')) {
                int Count = 0;
                var Variable = new Variable("");

                do {
                    var subText = Name.FindMatchingBrackets("[", "]");
                    Variable.Add(
                        This._Parse(subText.Trim())
                    );

                    Name = Name.Replace("[" + subText + "]", "{" + Count.ToString() + "}");
                    Count++;
                } while (Name.IndexOf("[") < Name.IndexOf("]"));

                Variable.Name = Name;

                return Variable;
            }
            else {
                return new Variable(Name);
            }
        }

        public static Expression __String(Engine This, string Text, ref int Index) {
            if (Text.Compare("\"", Index)) {
                var Temp = Text.FindMatchingBrackets("\"", "\"", Index);

                Index += Temp.Length + 2;

                return new Expression(new String(Temp));
            }
            else if (Text.Compare("'", Index)) {
                var Temp = Text.FindMatchingBrackets("'", "'", Index);

                Index += Temp.Length + 2;

                return new Expression(new String(Temp));
            }
            else {
                return null;
            }
        }

        public static Expression __Number(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            ConsumeWhitespace(Text, ref Index);

            var Value = "";

            if (Text.IndexOf(";", Index) == -1)
                Value = Text.Substring(Index);
            else
                Value = Text.Substring(Index, Text.IndexOf(";", Index) - Index);

            if (Value.IsNumeric()) {
                Index += Value.Length;

                return new Expression(
                    new Number(double.Parse(Value.Trim()))
                );
            }
            return null;
        }

        public static Expression __Boolean(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            ConsumeWhitespace(Text, ref Index);

            var Value = "";

            if (Text.IndexOf(";", Index) == -1)
                Value = Text.Substring(Index);
            else
                Value = Text.Substring(Index, Text.IndexOf(";", Index) - Index);

            if (Value.IsBoolean()) {
                Index += Value.Length;

                return new Expression(
                    new Bool(bool.Parse(Value.Trim()))
                );
            }
            return null;
        }

        public static Expression __Variable(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            ConsumeWhitespace(Text, ref Index);

            var Name = "";

            if (Text.IndexOf(";", Index) == -1)
                Name = Text.Substring(Index);
            else
                Name = Text.Substring(Index, Text.IndexOf(";", Index) - Index);
            

            if (Name == "break") {
                Index += Name.Length + 1;
                return new Break();
            }

            if (Name == "continue") {
                Index += Name.Length + 1;
                return new Continue();
            }

            if (!_isValidVariableName(Name))
                return null;

            Index += Name.Length + 1;

            return new Expression(__ParseVar(This, Name));
        }

        public static Expression __If(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("if", Index)) {
                return null;
            }

            var SubIndex = Index + 2;
            ConsumeWhitespace(Text, ref SubIndex);

            if (!Text.Compare("(", SubIndex)) {
                return null;
            }

            var Expression = new If();
            var Temp = string.Empty;

            Temp = Text.FindMatchingBrackets("(", ")", SubIndex);

            SubIndex += Temp.Length + 2;
            ConsumeWhitespace(Text, ref SubIndex);

            Expression.Boolean = This._Parse(Temp.Trim());
            This._parseExpression(Text, ref SubIndex).CopyTo(Expression);
            ConsumeWhitespace(Text, ref SubIndex);

            if (Text.Compare("else", SubIndex)) {
                SubIndex += 4;
                ConsumeWhitespace(Text, ref SubIndex);
                Expression.Else = This._parseExpression(Text, ref SubIndex);
            }

            Index = SubIndex;

            return Expression;
        }

        public static Expression __For(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("for", Index))
                return null;

            var SubIndex = Index + 3;
            ConsumeWhitespace(Text, ref SubIndex);

            if (!Text.Compare("(", SubIndex))
                return null;

            var Expression = new For();
            var Temp = string.Empty;

            SubIndex += 1;

            Temp = Text.Substring("", ";", SubIndex);

            Expression.Init = This._Parse(Temp.Trim());
            SubIndex += Temp.Length + 1;

            Temp = Text.Substring("", ";", SubIndex);
            Expression.Boolean = This._Parse(Temp.Trim());
            SubIndex += Temp.Length + 1;

            Temp = Text.Substring("", ")", SubIndex);
            Expression.Modifier = This._Parse(Temp.Trim());
            SubIndex += Temp.Length + 1;

            ConsumeWhitespace(Text, ref SubIndex);

            if (Text.Compare("{", SubIndex)) {
                Temp = Text.FindMatchingBrackets("{", "}", SubIndex);
                SubIndex += Temp.Length + 2;

                This._Parse(Temp.Trim()).CopyTo(Expression);
            }
            else {
                Temp = Text.Substring("", ";", SubIndex);
                SubIndex += Temp.Length + 1;

                This._Parse(Temp.Trim()).CopyTo(Expression);
            }

            Index = SubIndex;

            return Expression;
        }

        public static Expression __Foreach(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("foreach", Index))
                return null;

            var SubIndex = Index + 7;
            ConsumeWhitespace(Text, ref SubIndex);

            if (!Text.Compare("(", SubIndex))
                return null;

            var Expression = new Foreach();
            var Temp = string.Empty;

            SubIndex += 1;
            ConsumeWhitespace(Text, ref SubIndex);

            Temp = Text.Substring("", "in", SubIndex);

            if (string.IsNullOrEmpty(Temp))
                return null;

            SubIndex += Temp.Length + 2;
            ConsumeWhitespace(Text, ref SubIndex);

            Expression.VarName = Temp.Trim();

            Temp = Text.Substring("", ")", SubIndex);

            Expression.Object = This._Parse(Temp.Trim());

            SubIndex += Temp.Length + 1;
            ConsumeWhitespace(Text, ref SubIndex);
            
            if (string.IsNullOrEmpty(Temp))
                return null;

            if (Text.Compare("{", SubIndex)) {
                Temp = Text.FindMatchingBrackets("{", "}", SubIndex);
                SubIndex += Temp.Length + 2;
            }
            else {
                Temp = Text.Substring("", ";");
                SubIndex += Temp.Length + 1;
            }

            ConsumeWhitespace(Text, ref SubIndex);
            This._Parse(Temp.Trim()).CopyTo(Expression);

            Index = SubIndex;

            return Expression;
        }

        public static Expression __While(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("while", Index))
                return null;

            Index += 5;
            ConsumeWhitespace(Text, ref Index);

            if (!Text.Compare("(", Index))
                return null;

            var Expression = new While();
            var Temp = string.Empty;

            Temp = Text.FindMatchingBrackets("(", ")", Index);

            Index += Temp.Length + 2;
            Expression.Boolean = This._Parse(Temp);

            ConsumeWhitespace(Text, ref Index);

            if (Text.Compare("{", Index)) {
                Temp = Text.FindMatchingBrackets("{", "}", Index);
                Index += Temp.Length + 2;

                This._Parse(Temp.Trim()).CopyTo(Expression);
            }
            else {
                Temp = Text.Substring("", ";", Index);
                Index += Temp.Length;

                This._Parse(Temp.Trim()).CopyTo(Expression);
            }

            return Expression;
        }

        public static Expression __Do(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("do", Index))
                return null;

            Index += 2;
            ConsumeWhitespace(Text, ref Index);

            if (!Text.Compare("{", Index)) {
                return null;
            }

            var Expression = new Do();
            var Temp = string.Empty;

            Temp = Text.FindMatchingBrackets("{", "}", Index);

            Index += Temp.Length + 2;
            ConsumeWhitespace(Text, ref Index);

            This._Parse(Temp.Trim()).CopyTo(Expression);

            if (!Text.Compare("while", Index))
                return null;

            Index += 5;
            ConsumeWhitespace(Text, ref Index);

            if (!Text.Compare("(", Index))
                return null;

            Temp = Text.FindMatchingBrackets("(", ")", Index);

            Index += Temp.Length + 2;
            Expression.Boolean = This._Parse(Temp);
            ConsumeWhitespace(Text, ref Index);

            if (!Text.Compare(";", Index))
                return null;
            else
                Index++;

            ConsumeWhitespace(Text, ref Index);

            return Expression;
        }

        public static Expression __Unary(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            var SubIndex = Index;

            ConsumeContent(Text, ref SubIndex);

            if (SubIndex == Index || SubIndex >= Text.Length)
                return null;

            var Left = Text.Substring(Index, SubIndex - Index);

            ConsumeWhitespace(Text, ref SubIndex);

            if (Text.Compare("+=", SubIndex)) {
                var Variable = __ParseVar(This, Left.Trim());

                Index = SubIndex + 2;
                ConsumeWhitespace(Text, ref Index);

                return new Assign(
                    Variable,
                    new Add(
                        Variable,
                        This._parseExpression(Text, ref Index)
                    )
                );
            }
            else if (Text.Compare("-=", SubIndex)) {
                var Variable = __ParseVar(This, Left.Trim());

                Index = SubIndex  + 2;
                ConsumeWhitespace(Text, ref Index);

                return new Assign(
                    Variable,
                    new Subtract(
                        Variable,
                        This._parseExpression(Text, ref Index)
                    )
                );
            }
            else if (Text.Compare("*=", SubIndex)) {
                var Variable = __ParseVar(This, Left.Trim());

                Index = SubIndex + 2;
                ConsumeWhitespace(Text, ref Index);

                return new Assign(
                    Variable,
                    new Multiply(
                        Variable,
                        This._parseExpression(Text, ref Index)
                    )
                );
            }
            else if (Text.Compare("/=", SubIndex)) {
                var Variable = __ParseVar(This, Left.Trim());

                Index = SubIndex + 2;
                ConsumeWhitespace(Text, ref Index);

                return new Assign(
                    Variable,
                    new Devide(
                        Variable,
                        This._parseExpression(Text, ref Index)
                    )
                );
            }
            else if (Text.Compare("++", SubIndex)) {
                Index = SubIndex + 2;
                ConsumeWhitespace(Text, ref Index);

                var Variable = __ParseVar(This, Left.Trim());
                return new Assign(
                    Variable,
                    new Add(
                        Variable,
                        new Number(1)
                    )
                );
            }
            else if (Text.Compare("--", SubIndex)) {
                Index = SubIndex + 2;
                ConsumeWhitespace(Text, ref Index);

                var Variable = __ParseVar(This, Left.Trim());
                return new Assign(
                    Variable,
                    new Subtract(
                        Variable,
                        new Number(1)
                    )
                );
            }
            else if (Text.Compare("==", SubIndex)) {
                Index = SubIndex + 2;
                ConsumeWhitespace(Text, ref Index);

                return new Equal(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }
            else if (Text.Compare("!=", SubIndex)) {
                Index = SubIndex + 2;
                ConsumeWhitespace(Text, ref Index);

                return new NotEqual(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }
            else if (Text.Compare("<=", SubIndex)) {
                Index = SubIndex + 2;
                ConsumeWhitespace(Text, ref Index);

                return new LessThanEqualTo(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }
            else if (Text.Compare(">=", SubIndex)) {
                Index = SubIndex + 2;
                ConsumeWhitespace(Text, ref Index);

                return new GreaterThanEqualTo(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }
            else if (Text.Compare("<", SubIndex)) {
                Index = SubIndex + 1;
                ConsumeWhitespace(Text, ref Index);

                return new LessThan(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }
            else if (Text.Compare(">", SubIndex)) {
                var Right = Text.Substring(SubIndex + 1);

                Index = SubIndex + 1;
                ConsumeWhitespace(Text, ref Index);

                return new GreaterThan(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }
            else if (Text.Compare("=", SubIndex)) {
                var Variable = __ParseVar(This, Left.Trim());

                Index = SubIndex + 1;
                ConsumeWhitespace(Text, ref Index);

                var Value = This._parseExpression(Text, ref Index);

                return new Assign(
                    Variable,
                    Value
                );
            }
            else if (Text.Compare("+", SubIndex)) {
                Index = SubIndex + 1;
                ConsumeWhitespace(Text, ref Index);

                return new Add(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }
            else if (Text.Compare("-", SubIndex)) {
                Index = SubIndex + 1;
                ConsumeWhitespace(Text, ref Index);

                return new Subtract(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }
            else if (Text.Compare("*", SubIndex)) {
                Index = SubIndex + 1;
                ConsumeWhitespace(Text, ref Index);

                return new Multiply(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }
            else if (Text.Compare("/", SubIndex)) {
                Index = SubIndex + 1;
                ConsumeWhitespace(Text, ref Index);

                return new Devide(
                    This._Parse(Left.Trim()),
                    This._parseExpression(Text, ref Index)
                );
            }

            return null;
        }

        public static Expression __Return(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("return", Index))
                return null;

            var SubIndex = Index + 6;
            ConsumeWhitespace(Text, ref SubIndex);

            if (Text.IndexOf(';', SubIndex) == -1) {
                Index = SubIndex;
                return null;
            }

            var Right = Text.Substring(SubIndex, Text.IndexOf(';', SubIndex) - SubIndex);

            SubIndex += Right.Length + 1;

            var Value = This._Parse(Right);

            Index = SubIndex;

            return new Return(
                Value
            );
        }

        public static Expression __Delete(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("delete", Index))
                return null;

            var SubIndex = Index + 6;
            ConsumeWhitespace(Text, ref SubIndex);

            if (Text.IndexOf(';', SubIndex) == -1)
                return null;

            var Right = Text.Substring(SubIndex, Text.IndexOf(';', SubIndex) - SubIndex);

            SubIndex += Right.Length + 1;

            var Expression = new Expression(
                new Delete(
                    Right.Trim()
                )
            );

            Index = SubIndex;

            return Expression;
        }

        public static Expression __CodeBlock(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("{", Index)) {
                return null;
            }

            var Temp = Text.FindMatchingBrackets("{", "}", Index);

            Index += Temp.Length + 2;

            return This._Parse(Temp);
        }

        public static Expression __EvalBlock(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("(", Index)) {
                return null;
            }

            var Temp = Text.FindMatchingBrackets("(", ")", Index);

            Index += Temp.Length + 2;

            return This._Parse(Temp);
        }

        public static Expression __Using(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("using", Index))
                return null;

            Index += 5;
            ConsumeWhitespace(Text, ref Index);

            if (!Text.Contains(";"))
                return null;

            var Name = Text.Substring("", ";", Index);

            Index += Name.Length + 1;

            if (!Library.Defined.ContainsKey(Name.Trim()))
                return null;

            This.Libraries.Add(Library.Defined[Name.Trim()]);

            return new Nop();
        }

        public static Expression __Async(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            var SubIndex = Index;

            if (!Text.Compare("async", Index))
                return null;

            SubIndex += 5;
            ConsumeWhitespace(Text, ref SubIndex);

            Expression Expression = This._parseExpression(Text, ref SubIndex);

            if (Expression != null) {
                Index = SubIndex;

                return new Async(Expression);
            }

            return null;
        }

        public static Expression __FunctionDefinition(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            if (!Text.Compare("function", Index))
                return null;

            var SubIndex = Index += 8;
            ConsumeWhitespace(Text, ref SubIndex);

            while (SubIndex < Text.Length) {
                char c = Text[SubIndex];

                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c > '0' && c < '9') || (c == '_') || (c == '.')) {
                    SubIndex++;
                }
                else {
                    break;
                }
            }

            var Name = Text.Substring(Index, SubIndex - Index);

            ConsumeWhitespace(Text, ref SubIndex);

            if (!Text.Compare("(", SubIndex)) {
                return null;
            }

            var Temp = Text.FindMatchingBrackets("(", ")", SubIndex);

            SubIndex += Temp.Length + 2;
            ConsumeWhitespace(Text, ref SubIndex);

            if (!Text.Compare("{", SubIndex)) {
                return null;
            }

            var Expression = new Function();

            Expression.Arguments.AddRange(
                Temp.ParseCParams()
            );

            Temp = Text.FindMatchingBrackets("{", "}", SubIndex);
            SubIndex += Temp.Length + 2;
            ConsumeWhitespace(Text, ref SubIndex);

            This._Parse(Temp).CopyTo(Expression);

            Index = SubIndex;

            if (!string.IsNullOrEmpty(Name)) {
                Expression.Name = Name.Trim();
                This.Functions[Name.Trim()] = Expression;
            }

            return Expression;
        }

        public static Expression __FunctionCall(Engine This, string Text, ref int Index) {
            if (This == null || string.IsNullOrEmpty(Text))
                return null;

            var SubIndex = Index;

            while (SubIndex < Text.Length) {
                char c = Text[SubIndex];

                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c > '0' && c < '9') || (c == '_') || (c == '.')) {
                    SubIndex++;
                }
                else {
                    break;
                }
            }

            if (SubIndex == Index)
                return null;

            var Name = Text.Substring(Index, SubIndex - Index);

            ConsumeWhitespace(Text, ref SubIndex);

            if (!Text.Compare("(", SubIndex)) {
                return null;
            }

            var RawArgs = Text.FindMatchingBrackets("(", ")", SubIndex);

            SubIndex += RawArgs.Length + 2;
            ConsumeWhitespace(Text, ref SubIndex);

            if (Text.Compare(";", SubIndex)) {
                SubIndex++;
                ConsumeWhitespace(Text, ref SubIndex);
            }

            var Args = RawArgs.ParseCParams();

            var Func = This.GetFunction(Name.Trim());
            Index = SubIndex;

            if (Func == null) {
                var Call = new ContextCall() {
                    Engine = This,
                    Name = Name.Trim()
                };

                for (int x = 0; x < Args.Length; x++) {
                    Call.Arguments.Add(This._Parse(Args[x]));
                }

                return Call;
            }
            else {
                var Call = new Call() {
                    Function = Func
                };

                if (Func.Arguments.Count != Args.Length)
                    return null;

                for (int x = 0; x < Func.Arguments.Count && x < Args.Length; x++) {
                    Call.Arguments[Func.Arguments[x]] = This._Parse(Args[x]);
                }

                return Call;
            }
        }
    }
}
