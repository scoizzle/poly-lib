﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;

namespace Poly.Script {
    public partial class Engine : jsObject {
        private static bool _evalBooleanNode(Node Node, jsObject Context) {
            var Val = Node.Evaluate(Context);

            if (Val is Bool) {
                return (Val as Bool).Value;
            }
            else if (Val is String) {
                return !string.IsNullOrEmpty((Val as String).Value);
            }
            else if (Val is Number) {
                return (Val as Number).Value != default(double);
            }
            else if (Val is Object) {
                return (!(Val as Object).IsEmpty);
            }
            return false;
        }

        private Expression _parseExpression(string Text, ref int Index) {
            ConsumeWhitespace(Text, ref Index);

            if (string.IsNullOrEmpty(Text)) {
                return new Expression();
            }

            foreach (var Parser in ExpressionParsers) {
                var Ret = Parser(this, Text, ref Index);

                if (Ret != null) {
                    return Ret;
                }
            }

            var Expression = new Expression();

            if (Text.Compare(";", Index)) {
                Index++;
                ConsumeWhitespace(Text, ref Index);
                return _parseExpression(Text, ref Index);
            }
            else if (Index < Text.Length) {
                Text = Text.Substring(Index);
            }
            else {
                Text = "";
            }

            if (!string.IsNullOrEmpty(Text)) {
                Application.Log.Error("Input Text was not in a valid format.\r\n" + Text);
            }

            Index += Text.Length;

            return Expression;
        }

        private Expression _Parse(string Text) {
            var Index = 0;
            return _Parse(Text, ref Index);
        }

        private Expression _Parse(string Text, ref int Index) {
            var Expression = new Expression();
            do {
                var Exp = _parseExpression(Text, ref Index);

                if (Exp is Function || Exp is Nop)
                    continue;

                Expression.Add(   
                    Exp
                );

                ConsumeWhitespace(Text, ref Index);
            }
            while (Index < Text.Length);

            return Expression;
        }

        public new bool Parse(string Text) {
            int Index = 0;

            if (Text == null) {
                return false;
            }
            else {
                Text = Text.Trim();
            }
            if (Text == string.Empty) {
                return true;
            }

            _Parse(Text, ref Index).CopyTo(SourceTree);

            return true;
        }
    }
}