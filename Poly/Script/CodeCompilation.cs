﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.CSharp;

namespace Poly.Script {
    public class DynamicCodeCompiler {
        public object Instance;
        public Type   InstanceType;

        public CodeDomProvider CompilerInterface;
        public CompilerParameters CompilerParams;
        public CompilerResults CompilerResults;

        public string MainClass = string.Empty;
        public string EntryPoint = string.Empty;

        public DynamicCodeCompiler(string ClassName, bool Debug = false, params string[] Assemblies) {
            this.MainClass = ClassName;

            CompilerInterface = new CSharpCodeProvider();
            CompilerParams = new CompilerParameters(Assemblies) {
                MainClass = ClassName,
                GenerateInMemory = true,
                IncludeDebugInformation = Debug
            };

            CompilerParams.ReferencedAssemblies.Add("System.dll");
            CompilerParams.ReferencedAssemblies.Add("System.Core.dll");
            CompilerParams.ReferencedAssemblies.Add("System.Data.dll");
            CompilerParams.ReferencedAssemblies.Add("System.Xml.dll");
            CompilerParams.ReferencedAssemblies.Add("System.Xml.Linq.dll");
            CompilerParams.ReferencedAssemblies.Add("Poly.dll");
        }

        public virtual bool Compile(params string[] Source) {
            CompilerResults = CompilerInterface.CompileAssemblyFromSource(CompilerParams, Source);

            return !CompilerResults.Errors.HasErrors;
        }

        public object Run(params object[] Args) {
            return Run(EntryPoint, Args);
        }

        public object Run(string MethodName, params object[] Args) {
            if (Instance == null) {
                Instance = CompilerResults.CompiledAssembly.CreateInstance(MainClass, true);
                InstanceType = Instance.GetType();
            }

            return InstanceType.InvokeMember(MethodName, BindingFlags.InvokeMethod, null, Instance, Args);
        }
    }
}
