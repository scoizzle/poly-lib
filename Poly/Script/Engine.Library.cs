﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using Poly;
using Poly.Data;

namespace Poly.Script {
    public partial class Engine : jsObject {
        public class Library : jsObject<Function> {
            public void Add(Function Func) {
                if (Func != null && !string.IsNullOrEmpty(Func.Name))
                    this.Add(Func.Name, Func);
            }

            public Function GetFunction(string Name) {
                var Func = this[Name];

                if (Func != null && Func is Function)
                    return (Function)(Func);

                return null;
            }

            public static void RegisterLib(string Name, Library Lib) {
                Defined.Add(Name, Lib);
            }

            public static void RemoveLib(string Name) {
                Defined.Remove(Name);
            }

            public static jsObject<NetworkStream> NetConnections = new jsObject<NetworkStream>();
            public static jsObject<Thread> Threads = new jsObject<Thread>();

            public static string streamId(string Key) {
                return Key.MD5() + DateTime.Now.ToString().MD5();
            }

            public static jsObject<Library> Defined = new jsObject<Library>() {
                { "std", 
                    new Library() {
                        new SystemFunction("print", 
                            (Args) => {
                                Console.Write(Args["str"].ToString());
                                return new Void(); 
                            }, 
                            "str"
                        ),
                        new SystemFunction("printl", 
                            (Args) => {
                                Console.WriteLine(Args["str"].ToString());
                                return new Void(); 
                            }, 
                            "str"
                        ),
                        new SystemFunction("read", 
                            (Args) => {
                                return new String(char.ConvertFromUtf32(Console.Read()).ToString()); 
                            }
                        ),
                        new SystemFunction("readl", 
                            (Args) => {
                                return new String(Console.ReadLine()); 
                            }, 
                            "str"
                        ),
                        new SystemFunction("strlen", 
                            (Args) => { 
                                return new Number(Args["str"].ToString().Length); 
                            }, 
                            "str"
                        ),
                        new SystemFunction("strcmp", 
                            (Args) => { 
                                return new Number(string.Compare(Args["str1"].ToString(), Args["str2"].ToString())); 
                            }, 
                            "str1", "str2"
                        ),
                        new SystemFunction("strstr", 
                            (Args) => { 
                                return new Number(Args["str1"].ToString().IndexOf(Args["str2"].ToString())); 
                            }, 
                            "str1", "str2"
                        ),
                        new SystemFunction("substr", 
                            (Args) => {
                                var str = Args["str1"].ToString();
                                var index = Args.getDouble("index");

                                if (index >= str.Length)
                                    return new String("");

                                return new String(str.Substring((int)index));
                            }, 
                            "str1", "index"
                        ),
                        new SystemFunction("sleep",
                            (Args) => {
                                var Delay = (int)Args.getDouble("Delay");

                                System.Threading.Thread.Sleep(Delay);

                                return new Script.Engine.Void();
                            },
                            "Delay"
                        ),
                        new SystemFunction("num",
                            (Args) => {
                                return new Number(double.Parse(Args["txt"].ToString()));
                            },
                            "txt"
                        ),
                        new SystemFunction("str",
                            (Args) => {
                                return new String(Args["num"].ToString());
                            },
                            "num"
                        ),
                        new SystemFunction("humanify",
                            (Args) => {
                                return new String(
                                    jsObject.Stringify(
                                        Args.getObject("obj"), true
                                    )
                                );
                            },
                            "obj"
                        ),
                        new SystemFunction("escape",
                            (Args) => {
                                return new String(Args["str"].ToString().Escape());
                            },
                            "str"
                        ),
                        new SystemFunction("descape",
                            (Args) => {
                                return new String(Args["str"].ToString().Descape());
                            },
                            "str"
                        )
                    }
                },
                { "math",
                    new Library() {
                        new SystemFunction("sqrt", 
                            (Args) => {
                                return new Number(Math.Sqrt(Args.getDouble("val")));
                            }, 
                            "val"
                        ),
                        new SystemFunction("pow", 
                            (Args) => {
                                return new Number(Math.Pow(Args.getDouble("base"), Args.getDouble("exp")));
                            }, 
                            "base", "exp"
                        ),
                        new SystemFunction("min", 
                            (Args) => {
                                return new Number(Math.Min(Args.getDouble("val1"), Args.getDouble("val2")));
                            }, 
                            "val1", "val2"
                        ),
                        new SystemFunction("max", 
                            (Args) => {
                                return new Number(Math.Max(Args.getDouble("val1"), Args.getDouble("val2")));
                            }, 
                            "val1", "val2"
                        ),
                        new SystemFunction("round", 
                            (Args) => {
                                return new Number(Math.Round(Args.getDouble("val1"), (int)Args.getDouble("val2")));
                            }, 
                            "val1", "val2"
                        )
                    }
                },
                { "log",
                    new Library() {
                        new SystemFunction("log",
                            (Args) => {
                                Application.Log.Info(Args["msg"].ToString());
                                return new Void();
                            },
                            "msg"
                        ),
                        new SystemFunction("log.info",
                            (Args) => {
                                Application.Log.Info(Args["msg"].ToString());
                                return new Void();
                            },
                            "msg"
                        ),
                        new SystemFunction("log.error",
                            (Args) => {
                                Application.Log.Error(Args["msg"].ToString());
                                return new Void();
                            },
                            "msg"
                        ),
                        new SystemFunction("log.warning",
                            (Args) => {
                                Application.Log.Warning(Args["msg"].ToString());
                                return new Void();
                            },
                            "msg"
                        ),
                        new SystemFunction("log.fatal",
                            (Args) => {
                                Application.Log.Fatal(Args["msg"].ToString());
                                return new Void();
                            },
                            "msg"
                        )
                    }
                },
                { "crypto",
                    new Library() {
                        new SystemFunction("md5",
                            (Args) => {
                                return new String(Args["obj"].ToString().MD5());
                            },
                            "obj"
                        ),
                        new SystemFunction("sha256",
                            (Args) => {
                                return new String(Args["obj"].ToString().SHA256());
                            },
                            "obj"
                        ),
                        new SystemFunction("sha512",
                            (Args) => {
                                return new String(Args["obj"].ToString().SHA512());
                            },
                            "obj"
                        )
                    }
                },
                { "file",
                    new Library() {
                        new SystemFunction("fread",
                            (Args) => {
                                var FileName = Args["FileName"].ToString();

                                return new String(File.ReadAllText(FileName));
                            },
                            "FileName"
                        ),
                        new SystemFunction("fprint",
                            (Args) => {
                                var FileName = Args["FileName"].ToString();
                                var Content = Args["Text"].ToString();

                                File.WriteAllText(FileName, Content);

                                return new Void();
                            },
                            "FileName", "Text"
                        ),
                        new SystemFunction("fappend",
                            (Args) => {
                                var FileName = Args["FileName"].ToString();
                                var Content = Args["Text"].ToString();

                                File.AppendAllText(FileName, Content);

                                return new Void();
                            },
                            "FileName", "Text"
                        ),
                        new SystemFunction("fdelete",
                            (Args) => {
                                var FileName = Args["FileName"].ToString();

                                File.Delete(FileName);

                                return new Void();
                            },
                            "FileName"
                        )
                    }
                },
                { "net",
                    new Library() {
                        new SystemFunction("sopen",
                            (Args) => {
                                var Type = Args["Type"].ToString();
                                var Host = Args["Host"].ToString();
                                var Port = int.Parse(Args["Port"].ToString());
                                var Id = streamId(Type + Host + Port);

                                if (Type.Equals("tcp")) {
                                    var Client = new TcpClient(Host, Port);

                                    if (Client.Connected) {
                                        NetConnections[Id] = Client.GetStream();
                                        return new String(Id);
                                    }
                                }
                                else if (Type.Equals("udp")) {
                                    var Client = new UdpClient(Host, Port);

                                    NetConnections[Id] = new NetworkStream(Client.Client);
                                    return new String(Id);
                                }
                                return new String("");
                            },
                            "Type", "Host", "Port"
                        ),
                        new SystemFunction("stimeout",
                            (Args) => {
                                var Id = Args["Id"].ToString();
                                var Client = NetConnections[Id];
                                var Timeout = int.Parse(Args["Time"].ToString());

                                if (Client == null) {
                                    return new Bool(false);
                                }
                                else {
                                    Client.ReadTimeout = Timeout;
                                    Client.WriteTimeout = Timeout;
                                }
                                return new Bool(true);
                            },
                            "Id", "Time"
                        ),
                        new SystemFunction("sread",
                            (Args) => {
                                var Id = Args["Id"].ToString();
                                var Client = NetConnections[Id];

                                if (Client == null) {
                                    return new String("");
                                }
                                else {
                                    var Char = char.ConvertFromUtf32(Client.ReadByte());

                                    return new String(Char);
                                }
                            },
                            "Id"
                        ),
                        new SystemFunction("sreadl",
                            (Args) => {
                                var Id = Args["Id"].ToString();
                                var Client = NetConnections[Id];

                                if (Client == null) {
                                    return new String("");
                                }
                                else {
                                    var Output = new StringBuilder();
                                    
                                    if (Client.CanRead) {
                                        Output.Append(new StreamReader(Client).ReadLine());
                                    }

                                    return new String(Output.ToString());
                                }
                            },
                            "Id"
                        ),
                        new SystemFunction("sprint",
                            (Args) => {
                                var Id = Args["Id"].ToString();
                                var Client = NetConnections[Id];
                                var Data = Args["Data"].ToString();

                                if (Client == null) {
                                    return new Bool(false);
                                }
                                else {
                                    var Array = Encoding.UTF8.GetBytes(Data);

                                    Client.Write(Array, 0, Array.Length);
                                    Client.Flush();
                                }

                                return new Bool(true);
                            },
                            "Id", "Data"
                        ),
                        new SystemFunction("sprintl",
                            (Args) => {
                                var Id = Args["Id"].ToString();
                                var Client = NetConnections[Id];
                                var Data = Args["Data"].ToString() + "\r\n";

                                if (Client == null) {
                                    return new Bool(false);
                                }
                                else try {
                                    var Array = Encoding.UTF8.GetBytes(Data);

                                    Client.Write(Array, 0, Array.Length);
                                    Client.Flush();
                                } 
                                catch {
                                    return new Bool(false);
                                }

                                return new Bool(true);
                            },
                            "Id", "Data"
                        )
                    }
                }
            };
        }
    }
}
