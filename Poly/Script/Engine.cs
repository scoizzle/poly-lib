﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;

namespace Poly.Script {
    public partial class Engine : jsObject {
        public Expression SourceTree = new Expression();

        public Library Functions = new Library();
        public List<Library> Libraries = new List<Library>();

        public Engine() {
            this.Libraries.Add(Library.Defined["std"]);
        }

        public Function GetFunction(string Name) {
            if (Functions.ContainsKey(Name))
                return Functions[Name];

            foreach (var Lib in Libraries) {
                if (Lib.ContainsKey(Name))
                    return Lib[Name];
            }

            return null;
        }

        public Function GetFunction(string Name, jsObject Context) {
            object obj = Context[Name];

            if (obj is Function) {
                return (Function)obj;
            }
            return GetFunction(Name);
        }

        public jsObject Execute() {
            var Data = new jsObject();
            SourceTree.Evaluate(Data);
            return Data;
        }

        public jsObject Execute(jsObject Context) {
            SourceTree.Evaluate(Context);
            return Context;
        }
    }
}
