﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Poly.Data;
using Poly.Net.Irc;
using Poly.Script;

namespace Poly.Bot {
    public class Program : Application {
        static void Main(string[] args) {
            Init(args);

            jsFile Config = new jsFile();                       

            var Bot = new Bot();

            Bot.Server = "vps.scoizzle.com";
            Bot.Port = 8888;
            Bot.Username = "PolyBot";
            Bot.Password = "niggabot";
            Bot.Nick = "PolyBot";
            Bot.Ident = "PolyBot";
            Bot.Realname = "PolyBot";

            Engine.Library.RegisterLib("PolyBot", Bot.Library);

            Bot.OnConnect += (D) => {
                Bot.JoinChannel("#kaos");
            };

            Bot.OnData += (D) => {
                Application.Log.Info(D.Message);
            };

            Bot.AddCommand("@{Script}", (C, U, A) => {
                var Args = new jsObject() {
                    { "Convo", C },
                    { "User", U },
                    { "Bot", Bot }
                };

                Bot.ScriptEngine.Libraries.Add(Bot.Library);


                lock (Bot.Mutex) {
                    Bot.ScriptEngine.Parse(A.getString("Script"));
                    Bot.ScriptEngine.Execute(Args);
                    Bot.ScriptEngine.SourceTree.Clear();
                }
            });

            Bot.AddCommand("#test", (C, U, A) => {
                Bot.ScriptEngine.Parse(@"
using PolyBot;
async { sleep(5000); msg('#kaos', 'Hi'); }
msg('Scoizzle', 'Hi.');
                ");

                Bot.ScriptEngine.Execute(Bot);
                Bot.ScriptEngine.SourceTree.Clear();
            });
            
            Bot.Start();

            while (Application.Running) {
                var Input = Console.ReadLine();

                if (string.IsNullOrEmpty(Input))
                    continue;

                if (Input.Compare("exit"))
                    Application.Exit(0);

                var Convo = (Conversation)null;

                if (Bot.Conversations.ContainsKey(Bot.Nick)) {
                    Convo = Bot.Conversations.getObject<Net.Irc.Conversation>(Bot.Nick);
                }
                else {
                    Convo = new Net.Irc.Conversation(Bot.Nick);
                }

                Bot.FindAndInvokeEvent(Convo, Bot, Input);
            }
        }
    }
}
