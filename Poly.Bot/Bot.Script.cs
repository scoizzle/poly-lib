﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;
using Poly.Net.Irc;
using Poly.Script;

namespace Poly.Bot {
    public partial class Bot : Client {
        public Engine ScriptEngine = new Engine();
        private Engine.Library Lib = null;

        public Engine.Library Library {
            get {
                if (Lib != null)
                    return Lib;

                Lib = new Engine.Library() {
                    new Engine.SystemFunction("msg",
                        (Args) => {
                            var msg = Args["msg"].ToString();
                            var target = Args["target"].ToString();

                            if ((msg.Length > 0 && !char.IsLetterOrDigit(msg[0])) || target.ToLower().Contains("serv"))
                                return new Engine.Void();

                            this.SendMessage(Args["target"].ToString(), Args["msg"].ToString());
                            return new Engine.Void();
                        },
                        "target", "msg"
                    ),
                    new Engine.SystemFunction("notice",
                        (Args) => {
                            this.SendNotice(Args["target"].ToString(), Args["msg"].ToString());
                            return new Engine.Void();
                        },
                        "target", "msg"
                    ),
                    new Engine.SystemFunction("join",
                        (Args) => {
                            this.JoinChannel(Args["chan"].ToString());
                            return new Engine.Void();
                        },
                        "chan"
                    ),
                    new Engine.SystemFunction("part",
                        (Args) => {
                            this.PartChannel(Args["chan"].ToString());
                            return new Engine.Void();
                        },
                        "chan"
                    ),
                    new Engine.SystemFunction("addChatCommand",
                        (Args) => {
                            Engine.Function Func = ScriptEngine.GetFunction(Args["function"].ToString());

                            if (Func != null) {
                                AddCommand(Args["cmd"].ToString(), (C, U, A) => {
                                    A.Add("Convo", C);
                                    A.Add("User", U);
                                    A.Add("Bot", this);
                                    Func.Evaluate(A);
                                });
                            }
                            return new Engine.Void();
                        },
                        "cmd", "function"
                    )
                };
                return Lib;
            }
        }
    }
}