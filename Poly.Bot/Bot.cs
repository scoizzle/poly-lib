﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;
using Poly.Net.Irc;

namespace Poly.Bot {
    public partial class Bot : Client {
        public delegate void CommandEvent(Conversation Convo, User Sender, jsObject Args);
        public jsObject CommandTable = new jsObject();

        public object Mutex = new object();

        public Bot() {
            this.OnChannelMessage += Bot_OnMessage;
            this.OnUserMessage += Bot_OnMessage;
        }

        private void Bot_OnMessage(Conversation Convo, User User, string Message) {
            if (string.IsNullOrEmpty(Message))
                return;

            FindAndInvokeEvent(Convo, User, Message);
        }

        public void FindAndInvokeEvent(Conversation Convo, User User, string Input) {
            lock (Mutex) {
                foreach (var pair in CommandTable.AsParallel()) {
                    var Args = Input.Match(pair.Key);

                    if (Args != null) {
                        (pair.Value as CommandEvent)(Convo, User, Args);
                        return;
                    }
                };
            }
        }

        public void AddCommand(string Regex, CommandEvent Event) {
            lock (Mutex) 
                CommandTable[Regex] = Event;
        }

        public void RemoveCommand(string Regex) {
            lock (Mutex) 
                CommandTable.Remove(Regex);
        }

        public void RemoveCommand(CommandEvent Event) {
            lock (Mutex) {
                CommandTable.ToList().ForEach((pair) => {
                    if ((CommandEvent)pair.Value == Event)
                        CommandTable.Remove(pair.Key);
                });
            }
        }
    }
}
