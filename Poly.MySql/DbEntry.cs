﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;
using MySql.Data;

namespace Poly.MySql {
    public class DbEntry : jsObject {
        public Database DB {
            get;
            set;
        }

        public static string GenerateSalt() {
            return (DateTime.Now.ToString() + DateTime.UtcNow.ToString()).SHA256();
        }

        public virtual bool Create() {
            return false;
        }

        public virtual bool Read() {
            return false;
        }

        public virtual bool Update() {
            return false;
        }
        public virtual bool Delete() {
            return false;
        }
    }
}
