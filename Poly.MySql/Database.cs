﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;
using MySql.Data.MySqlClient;

namespace Poly.MySql {
    public class Database {
        public MySqlConnection Connection;

        public Database(string Domain, string Database, string User, string Password) {
            var ConString = "SERVER={0};DATABASE={1};UID={2};PASSWORD={3};".ToString(
                Domain,
                Database,
                User,
                Password
            );

            Connection = new MySqlConnection(ConString);
        }

        ~Database() {
            Close();
        }

        public DbQuery Query {
            get {
                return new DbQuery(this);
            }
        }

        public bool Open() {
            try {
                Connection.Open();
            }
            catch (Exception Error) {
                Application.Log.Error(Error.ToString());
                return false;
            }
            return true;
        }

        public bool Close() {
            try {
                Connection.Close();
            }
            catch (Exception Error) {
                Application.Log.Error(Error.ToString());
                return false;
            }
            return true;
        }

        public bool Execute(string Query, params object[] Args) {
            try {
                var Command = new MySqlCommand(Query, Connection);

                for (int Index = 0; Index < Args.Length; Index++) {
                    Command.Parameters.AddWithValue(Index.ToString(), Args[Index]);
                }

                return (Command.ExecuteNonQuery() > 0);
            }
            catch (Exception Error) {
                Application.Log.Error(Error.ToString());
                return false;
            }
        }

        public bool Execute(string Query, jsObject Args) {
            try {
                var Command = new MySqlCommand(Query, Connection);

                foreach (string Key in Args.Keys) {
                    Command.Parameters.AddWithValue(Key, Args[Key]);
                }

                return Command.ExecuteNonQuery() > 0;
            }
            catch (Exception Error) {
                Application.Log.Error(Error.ToString());
                return false;
            }
        }

        public MySqlDataReader Read(string Query, params object[] Args) {
            try {
                var Command = new MySqlCommand(Query, Connection);

                for (int Index = 0; Index < Args.Length; Index++) {
                    Command.Parameters.AddWithValue(Index.ToString(), Args[Index]);
                }

                return Command.ExecuteReader();
            }
            catch (Exception Error) {
                Application.Log.Error(Error.ToString());
                return null;
            }
        }

        public MySqlDataReader Read(string Query, jsObject Args) {
            try {
                var Command = new MySqlCommand(Query, Connection);

                foreach (var Pair in Args) {
                    Command.Parameters.AddWithValue(Pair.Key, Pair.Value);
                }

                return Command.ExecuteReader();
            }
            catch (Exception Error) {
                Application.Log.Error(Error.ToString());
                return null;
            }
        }
    }
}
