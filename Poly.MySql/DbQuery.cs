﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Poly;
using Poly.Data;
using MySql.Data.MySqlClient;

namespace Poly.MySql {
    public class DbQuery : jsObject {
        public Database Db = null;
        public StringBuilder Output = new StringBuilder();

        public DbQuery(Database Database = null) {
            this.Db = Database;
        }

        public DbQuery Select(params string[] Cols) {
            Output.Append("SELECT ");

            if (Cols.Length > 0) {
                Output.AppendFormat("({0}) ", string.Join(",", Cols));
            }
            else {
                Output.Append("* ");
            }

            return this;
        }

        public DbQuery Insert(string Table) {
            Output.AppendFormat("INSERT INTO {0} ", Table);
            return this;
        }

        public DbQuery Update(string Table) {
            Output.AppendFormat("UPDATE {0} ", Table);
            return this;
        }

        public DbQuery Delete(string Table) {
            Output.AppendFormat("DELETE FROM {0} ");
            return this;
        }

        public DbQuery From(string Table) {
            Output.AppendFormat("FROM {0} ", Table);
            return this;
        }

        public DbQuery Where(params string[] Selectors) {
            Output.AppendFormat("WHERE ({0}) ", string.Join(", ", Selectors));
            return this;
        }

        public new DbQuery Values(params string[] Values) {
            Output.AppendFormat("VALUES ({0})", string.Join(",", Values));
            return this;
        }

        public bool Execute(params object[] Args) {
            return Db.Execute(Output.ToString(), Args);
        }

        public bool Execute(jsObject Args) {
            return Db.Execute(Output.ToString(), Args);
        }

        public jsObject Read(jsObject Args) {
            jsObject Obj = new jsObject();

            using (var Reader = Db.Read(Output.ToString(), Args)) {
                if (Reader == null)
                    return "";

                while (Reader.Read()) {
                    for (int Index = 0; Index < Reader.FieldCount; Index++) {
                        Obj[Reader.GetName(Index)] = Reader.GetValue(Index);
                    }
                }
            }

            return Obj;
        }

        public jsObject Read(params object[] Args) {
            jsObject Obj = new jsObject();

            using (var Reader = Db.Read(Output.ToString(), Args)) {
                if (Reader == null)
                    return "";

                while (Reader.Read()) {
                    for (int Index = 0; Index < Reader.FieldCount; Index++) {
                        Obj[Reader.GetName(Index)] = Reader.GetValue(Index);
                    }
                }
            }

            return Obj;
        }

        public jsObject ReadArray(jsObject Args) {
            jsObject Obj = new jsObject();

            using (var Reader = Db.Read(Output.ToString(), Args)) {
                if (Reader == null)
                    return "";

                int Row = 0;
                while (Reader.Read()) {
                    for (int Index = 0; Index < Reader.FieldCount; Index++) {
                        Obj[Row.ToString(), Reader.GetName(Index)] = Reader.GetValue(Index);
                    }
                    Row++;
                }
            }

            return Obj;
        }

        public jsObject ReadArray(params object[] Args) {
            jsObject Obj = new jsObject();

            using (var Reader = Db.Read(Output.ToString(), Args)) {
                if (Reader == null)
                    return "";

                int Row = 0;
                while (Reader.Read()) {
                    for (int Index = 0; Index < Reader.FieldCount; Index++) {
                        Obj[Row.ToString(), Reader.GetName(Index)] = Reader.GetValue(Index);
                    }
                    Row++;
                }
            }

            return Obj;
        }

        public override string ToString() {
            return Output.ToString();
        }
    }
}
